﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Capa_entidad;


// Para contactar con la WEB API
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
namespace capa_datos
{
    public class Data
    {
        static HttpClient client = new HttpClient();
        private static Data dat;
        private  Data()
        {
            client.BaseAddress = new Uri("https://localhost:44384/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            client.Timeout = TimeSpan.FromMinutes(10);
        }

        public static Data getData()
        {
            if (dat == null)
                dat = new Data();
            return dat;
        }

        public List<Article> readArticles()
        {
            List<Article> articles = null;
            string aux;

            try
            {
                HttpResponseMessage response = client.GetAsync("api/articulos").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    articles = JsonConvert.DeserializeObject<List<Capa_entidad.Article>>(aux);
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ESPECIAL ERROR:" + e);
            }

            return articles;
        }


        public List<Stock> readStock()
        {
            List<Stock> stock = null;
            string aux;

            try
            {
                HttpResponseMessage response = client.GetAsync("api/stock").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    stock = JsonConvert.DeserializeObject<List<Stock>>(aux);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR:" + e);
            }

            return stock;
        }

        // Leo todos los usuarios dela BD
        public List<User> readUsers()
        {
            List<User> listaUsuarios = null;
            string aux;

            try
            {
                HttpResponseMessage response = client.GetAsync("api/usuarios").Result;
                
                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    listaUsuarios = JsonConvert.DeserializeObject<List<User>>(aux);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine( e);
            }

            return listaUsuarios;
        }
        
        public List<Linpedido> readLinpedid()
        {
            List<Linpedido> linped = null;
            string aux;

            try
            {
                HttpResponseMessage response = client.GetAsync("api/Linpeds").Result;
                
                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    linped = JsonConvert.DeserializeObject<List<Linpedido>>(aux);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine( e);
            }

            return linped;
        }

        public List<Order> readorders()
        {
            List<Order> orders = null;
            string aux;

            try
            {
                HttpResponseMessage response = client.GetAsync("api/pedidos").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    orders = JsonConvert.DeserializeObject<List<Order>>(aux);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return orders;
        }

        public bool newLinPed(Linpedido l)
        {
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync("api/linped", l).Result;
                //Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }

            return false;
        }

        public bool newOrder(Order o)
        {
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync("api/pedidos", o).Result;
                //Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }

            return false;
        }

        // Creo un nuevo usuario en la BD
        public bool newUser(User u)
        {
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync("api/usuarios", u).Result;
                //Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }

            return false;
        }

        public bool editUser(User u)
        {
            try
            {
               //client.PutAsJsonAsync
                HttpResponseMessage response = client.PutAsJsonAsync("api/usuarios/"+u.usuarioID, u).Result;
                //Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }

            return false;
        }


        public bool editPrice(Article a)
        {
            try
            {
                //client.PutAsJsonAsync
                String url = "api/articulos/" + a.articuloID;
                HttpResponseMessage response = client.PutAsJsonAsync(url, a).Result;
                //Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }

            return false;
        }

        public List<Province> readProvinces()
        {
            List<Province> prov = null;
            string aux;
            try
            {
                HttpResponseMessage response = client.GetAsync("api/provincias").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    prov = JsonConvert.DeserializeObject<List<Province>>(aux);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return prov;
        }

        public List<Tv> readTvs()
        {
            List<Tv> tvs = null;
            string aux;
            try
            {
                HttpResponseMessage response = client.GetAsync("api/tvs").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    tvs = JsonConvert.DeserializeObject<List<Tv>>(aux);
                    int i = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return tvs;
        }

        public List<Memory> readMemory()
        {
            List<Memory> mem = null;
            string aux;
            try
            {
                HttpResponseMessage response = client.GetAsync("api/memorias").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    mem = JsonConvert.DeserializeObject<List<Memory>>(aux);
                    int i = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return mem;
        }

        public List<Camera> readCameras()
        {
            List<Camera> cam = null;
            string aux;
            try
            {
                HttpResponseMessage response = client.GetAsync("api/camaras").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    cam = JsonConvert.DeserializeObject<List<Camera>>(aux);
                    int i = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return cam;
        }

        public List<Lens> readLens()
        {
            List<Lens> lens = null;
            string aux;
            try
            {
                HttpResponseMessage response = client.GetAsync("api/objetivos").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    lens = JsonConvert.DeserializeObject<List<Lens>>(aux);
                    int i = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return lens;
        }

        public List<ArticleType> readArticleTypes()
        {
            List<ArticleType> articleTypes = null;
            string aux;
            try
            {
                HttpResponseMessage response = client.GetAsync("api/tipoarticulos").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    articleTypes = JsonConvert.DeserializeObject<List<ArticleType>>(aux);
                    int i = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return articleTypes;
        }

        public bool deleteUser(User u)
        {
            try
            {
                //client.PutAsJsonAsync
                HttpResponseMessage response = client.DeleteAsync("api/usuarios/" + u.usuarioID).Result;
                //Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }

            return false;
        }

        public bool deleteOrder(String id)
        {
            try
            {
                //client.PutAsJsonAsync
                HttpResponseMessage response = client.DeleteAsync("api/pedidos/" + id).Result;
                //Console.WriteLine(response);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e);
            }

            return false;
        }


        public List<Village> readVillages()
        {
            List<Village> prov = null;
            string aux;
            try
            {
                HttpResponseMessage response = client.GetAsync("api/localidades").Result;

                if (response.IsSuccessStatusCode)
                {
                    aux = response.Content.ReadAsStringAsync().Result;

                    prov = JsonConvert.DeserializeObject<List<Village>>(aux);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine( e);
            }

            return prov;
        }

    }
}
