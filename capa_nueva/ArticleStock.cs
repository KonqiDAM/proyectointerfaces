﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class ArticleStock
    {
        public ArticleStock(string articuloID, string nombre, string pvp, string marcaID, string imagen, string urlimagen, string especificaciones, string tipoArticuloID, int stock)
        {
            this.articuloID = articuloID;
            this.nombre = nombre;
            if(pvp != null)
                this.pvp = Convert.ToInt32(pvp);
            this.marcaID = marcaID;
            this.imagen = imagen;
            this.urlimagen = urlimagen;
            this.especificaciones = especificaciones;
            if(tipoArticuloID != null)
                this.tipoArticuloID = Convert.ToInt64(tipoArticuloID);
            this.stock = stock;
        }

        public string articuloID { get; set; }
        public string nombre { get; set; }
        public int pvp { get; set; }
        public string marcaID { get; set; }
        public string imagen { get; set; }
        public string urlimagen { get; set; }
        public string especificaciones { get; set; }
        public long tipoArticuloID { get; set; }

        public int stock { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.articuloID == ((Article)obj).articuloID;
        }

    }


}
