﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class Lens
    {
        public Lens(string objetivoID, string tipo, string montura, string focal, string apertura, string especiales)
        {
            ObjetivoID = objetivoID;
            this.tipo = tipo;
            this.montura = montura;
            this.focal = focal;
            this.apertura = apertura;
            this.especiales = especiales;
        }

        public string ObjetivoID { get; set; }
        public string tipo { get; set; }
        public string montura { get; set; }
        public string focal { get; set; }
        public string apertura { get; set; }
        public string especiales { get; set; }
    }
}
