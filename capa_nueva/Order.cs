﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class Order
    {
        public Order(int pedidoID, int usuarioID, string fecha)
        {
            PedidoID = pedidoID;
            this.usuarioID = usuarioID;
            this.fecha = fecha;
        }

        public int PedidoID { get; set; }
        public int usuarioID { get; set; }
        public string fecha { get; set; }
    }
}
