﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class ShipAddress
    {
        public ShipAddress(long usuarioID, string calle, string calle2, string codpos, string localidadID, string provinciaID)
        {
            this.usuarioID = usuarioID;
            this.calle = calle;
            this.calle2 = calle2;
            this.codpos = codpos;
            this.localidadID = localidadID;
            this.provinciaID = provinciaID;
        }

        public long usuarioID { get; set; }
        public string calle { get; set; }
        public string calle2 { get; set; }
        public string codpos { get; set; }
        public string localidadID { get; set; }
        public string provinciaID { get; set; }
    }
}
