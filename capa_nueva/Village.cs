﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class Village
    {
        public Village(string localidadID, string nombre, string provinciaID)
        {
            this.localidadID = localidadID;
            this.nombre = nombre;
            this.provinciaID = provinciaID;
        }

        public string localidadID { get; set; }
        public string nombre { get; set; }
        public string provinciaID { get; set; }

        // Para borrar dups
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj.GetType() == typeof(Village))
            {
                if (((Village)obj).localidadID == this.localidadID)
                    return true;
            }
                return false;
        }


    }
}
