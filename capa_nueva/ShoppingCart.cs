﻿using System;

namespace Capa_entidad
{
    public class ShoppingCart
    {
        public ShoppingCart(string articuloID, string usuarioID, string fecha)
        {
            this.articuloID = articuloID;
            this.usuarioID = usuarioID;
            this.fecha = fecha;
        }

        public string articuloID { get; set; }
        public string usuarioID { get; set; }
        public string fecha { get; set; }
    }
}
