﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class Linpedido
    {
        public Linpedido(int PedidoID, int linea, string articuloID, int importe, int cantidad)
        {
            this.PedidoID = PedidoID;
            this.linea = linea;
            this.articuloID = articuloID;
            this.importe = importe;
            this.cantidad = cantidad;
        }

        public int PedidoID  { get; set; }
        public int linea { get; set; }
        public string articuloID { get; set; }
        public int importe { get; set; }
        public int cantidad { get; set; }
    }
}
