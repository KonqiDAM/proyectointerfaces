﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class Memory
    {
        public Memory(string memoriaID, string tipo)
        {
            MemoriaID = memoriaID;
            this.tipo = tipo;
        }

        public string MemoriaID { get; set; }
        public string tipo { get; set; }
    }
}
