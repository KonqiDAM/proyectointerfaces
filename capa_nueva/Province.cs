﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class Province
    {
        public Province(string provinciaID, string nombre)
        {
            ProvinceID = provinciaID;
            this.nombre = nombre;
        }

        public string ProvinceID { get; set; }
        public string nombre { get; set; }
    }
}
