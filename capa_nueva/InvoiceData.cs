﻿using Capa_entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_entidad
{
    public class InvoiceData
    {
        public string articuloID { get; set; }
        public string nombre { get; set; }
        public int pvp { get; set; }
        public string marcaID { get; set; }
        public int PedidoID { get; set; }
        public int importe { get; set; }
        public int cantidad { get; set; }


        public InvoiceData(Linpedido linped, Article article)
        {
            this.articuloID = linped.articuloID;
            this.nombre = article.nombre;
            this.pvp = article.pvp;
            this.marcaID = article.marcaID;
            this.PedidoID = linped.PedidoID;
            this.importe = linped.importe;
            this.cantidad = linped.cantidad;
        }

    }
}
