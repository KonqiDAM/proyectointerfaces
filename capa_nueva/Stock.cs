﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class Stock
    {
        public Stock(string articuloID, int disponible, string entrega)
        {
            this.articuloID = articuloID;
            this.disponible = disponible;
            this.entrega = entrega;
        }

        public string articuloID { get; set; }
        public int disponible { get; set; }
        public string entrega { get; set; }
    }
}
