﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    public class ArticleType
    {
        public ArticleType(string tipoArticuloID, string descripcion)
        {
            this.tipoArticuloID = Convert.ToInt64(tipoArticuloID);
            this.Descripcion = descripcion;
            Console.WriteLine("debug:"+ tipoArticuloID);
            Console.WriteLine("debug:" + descripcion);

        }

        public long tipoArticuloID { get; set; }
        public string Descripcion { get; set; }
    }
}
