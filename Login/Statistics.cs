﻿using Capa_entidad;
using capa_negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class Statistics : Form
    {
        ToolStripStatusLabel statusLabel;
        List<Order> orders;
        List<Article> articles;
        List<Linpedido> linpedidos;
        Business neg;
        List<String> typesName;
        int[] days;
        int[] typesAmount;
        public Statistics(ToolStripStatusLabel statusLabel)
        {
            InitializeComponent();
            statusLabel.Text = "Loading";
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            this.statusLabel = statusLabel;
            chart1.Visible = false;
            chart1.Name = "Sales per day in month: ";
            neg = Business.getBusiness();
            orders = neg.getOrders();
            articles = neg.getArticles();
            linpedidos = neg.getLinped();
            typesName = new List<string>();
            typesName.Add("TV");
            typesName.Add("Memory");
            typesName.Add("Camera");
            typesName.Add("Lens");
            days = new int[31];
            typesAmount = new int[4];
            for (int i = 0; i < 12; i++)
            {
                comboBox1.Items.Add(CultureInfo.CurrentCulture.DateTimeFormat.MonthNames[i]);
            }
            statusLabel.Text = "Ready";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < days.Length; i++)
            {
                days[i] = 0;
            }
            for (int i = 0; i < typesAmount.Length; i++)
            {
                typesAmount[i] = 0;
            }
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();

            foreach (Order o in orders)
            {
                DateTime d = DateTime.Parse(o.fecha);
                
                if (d.Month == comboBox1.SelectedIndex+1)
                {
                    days[d.Day - 1]++;
                    List<Linpedido> aux = linpedidos.Where(x => x.PedidoID == o.PedidoID).ToList();
                    foreach(Linpedido l in aux)
                    {
                        foreach(Article a in articles)
                        {
                            if (a.articuloID == l.articuloID)
                                typesAmount[a.tipoArticuloID-1]++;
                        }
                    }
                }
            }
            for (int i = 0; i < days.Length; i++)
            {
                chart1.Series[0].Points.AddXY(i + 1, days[i]);
            }

            chart1.Titles.Clear();
            chart1.Titles.Add("Sales per day in month: " + comboBox1.Items[comboBox1.SelectedIndex]);
            chart1.Visible = true;
            chart2.Titles.Clear();
            chart2.Titles.Add("Sales types in month: " + comboBox1.Items[comboBox1.SelectedIndex]);
            chart2.Visible = true;

            for (int i = 0; i < typesAmount.Length; i++)
            {
                if(typesAmount[i] != 0)
                    chart2.Series[0].Points.AddXY(typesName[i], typesAmount[i]);
            }
            if(chart2.Series[0].Points.Count == 0)
                chart2.Series[0].Points.AddXY("No data", 100);
            chart2.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;

        }
    }
}
