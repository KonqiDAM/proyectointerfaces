﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using capa_negocio;
using Capa_entidad;

namespace Login
{
    public partial class InsertUser : Form
    {
        private ToolStripLabel toolStatus;
        private Business neg;
        private List<Province> provices;
        private List<Village> villages;
        private String IDletters;
        private bool isModify;
        private User aux;
        private bool deleteMode;
        private ModifyUser mod;

        public InsertUser(ToolStripLabel toolStatus)
        {
            toolStatus.Text = "Loading";
            toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
            deleteMode = false;
            isModify = false;
            InitializeComponent();
            dateTimePicker1.MaxDate = DateTime.Today;
            this.toolStatus = toolStatus;
            loadProvices();
            IDletters = "TRWAGMYFPDXBNJZSQVHLCKE";
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "dd-MM-yyyy";
            toolStatus.Text = "Ready";
        }
        public InsertUser(ToolStripLabel toolStatus, string id, ModifyUser mod) : this(toolStatus)
        {
            this.mod = mod;
            this.Hide();
            this.MdiParent = base.MdiParent;
            this.Show();
            aux = neg.getUsers().Where(e => e.usuarioID == id).ToArray()[0];
            textBox1.Text = aux.email;
            textBox2.Text = aux.nombre;
            textBox3.Text = aux.apellidos;
            textBox4.Text = aux.dni;
            textBox12.Text = aux.calle;
            textBox5.Text = aux.calle2;
            maskedTextBox1.Text = aux.telefono;
            maskedTextBox2.Text = aux.codpos;
            comboBox1.SelectedIndex = searchProvice(aux);
            comboBox2.SelectedIndex = searchVillage(aux);
            if(aux.nacido != null)
                dateTimePicker1.Value = DateTime.Parse( aux.nacido);
            button1.Text = "Save edit";
            isModify = true;
            textBox7.Text = aux.usuarioID;
            textBox6.Text = aux.password;
            textBox11.Text = aux.password;
            textBox6.Enabled = false;
            textBox11.Enabled = false;

            label13.Text = "User modification";
        }

        public void setDeleteMode(ModifyUser mod)
        {
            this.mod = mod;
            deleteMode = true;
            label13.Text = "User deletion";
            button2.Visible = true;
            button2.Enabled = true;
            button1.Text = "Delete this user";
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            textBox11.Enabled = false;
            textBox12.Enabled = false;
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            maskedTextBox1.Enabled = false;
            maskedTextBox2.Enabled = false;

            dateTimePicker1.Enabled = false;
        }

        private int searchProvice(User aux)
        {
            for (int i = 0; i < provices.Count; i++)
            {
                if (aux.provinciaID == provices[i].ProvinceID)
                    return i;
            }
            return 0;
        }

        private int searchVillage(User aux)
        {
            for (int i = 0; i < villages.Count; i++)
            {
                if (aux.puebloID == villages[i].localidadID)
                    return i;
            }
            return 0;
        }

        private void loadProvices()
        {
            this.toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
            this.toolStatus.Text = "Loading provinces";
            comboBox1.Text = "Select provinces";
            neg = Business.getBusiness();
            provices = neg.readProvinces();
            
            foreach (Province p in provices)
                comboBox1.Items.Add(p.nombre);
            this.toolStatus.Text = "Ready";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!deleteMode)
            {
                //Clear errors so it blinks again
                errorProvider1.Clear();
                //Validate all

                toolStatus.ForeColor = Color.FromArgb(118, 85, 163); ;
                toolStatus.Text = "Creating user....";
                if (validateAll())
                {

                    String idVillage = villages[comboBox2.SelectedIndex].localidadID;
                    String idProvice = provices[comboBox1.SelectedIndex].ProvinceID;
                    User tmp = new User(null,
                        textBox1.Text,
                        textBox6.Text,
                        textBox2.Text,
                        textBox3.Text,
                        textBox4.Text,
                        maskedTextBox1.Text,
                        textBox12.Text,
                        textBox5.Text,
                        maskedTextBox2.Text,
                        idVillage,
                        idProvice,
                        dateTimePicker1.Value.ToString("yyyy-MM-dd"));

                    if (isModify)
                    {
                        tmp.usuarioID = aux.usuarioID;

                        if (neg.editUser(tmp))
                        {
                            toolStatus.ForeColor = Color.FromArgb(118, 85, 163); ;
                            toolStatus.Text = "User updated correctly";
                            mod.populateTable();
                            this.Close();
                        }
                        else
                        {
                            toolStatus.ForeColor = Color.Red;
                            toolStatus.Text = "Error updating user";
                        }
                    }
                    else
                    {
                        if (neg.newUser(tmp))
                        {
                            toolStatus.ForeColor = Color.FromArgb(118, 85, 163); ;
                            toolStatus.Text = "User created correctly";
                            foreach (Control x in this.Controls)
                            {
                                if (x is TextBox)
                                {
                                    ((TextBox)x).Text = String.Empty;
                                }
                                if (x is MaskedTextBox)
                                {
                                    ((MaskedTextBox)x).Text = String.Empty;
                                }
                                if (x is ComboBox)
                                {
                                    ((ComboBox)x).Text = String.Empty;
                                }
                            }
                        }
                        else
                        {
                            toolStatus.ForeColor = Color.Red;
                            toolStatus.Text = "Error creating user";
                        }
                    }
                }
                else
                {
                    toolStatus.ForeColor = Color.Red;
                    toolStatus.Text = "Introduce mandatory fields";
                }
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this user?"
                    , "Delete", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (neg.deleteUser(aux))
                    {
                        toolStatus.ForeColor = Color.FromArgb(118, 85, 163); ;
                        toolStatus.Text = "User removed correctly";
                        mod.populateTable();
                        this.Close();
                    }
                    else
                    {
                        toolStatus.ForeColor = Color.Red;
                        toolStatus.Text = "User has orders! Can't be deleted!";
                    }
                }
            }
        }

        private bool validateAll()
        {
            validateID();
            validateEmail();
            validateVillage();
            validateName();
            validateSurname();
            validatePass();
            validateSecondsPass();

            return validateStreet()
                && validarCP() 
                && validateID() 
                && validateEmail() 
                && validateName() 
                && validateSurname() 
                && validatePass() 
                && validateNumber() 
                && validateSecondsPass()
                && validateVillage();
        }

        private bool validateVillage()
        {

            if( comboBox1.SelectedIndex == -1 )
            {
                errorProvider1.SetError(comboBox1, "Mandatory");
                errorProvider1.SetError(comboBox2, "Mandatory");

                return false;

            }
            else
            {
                errorProvider1.SetError(comboBox1, "");
            }
             if(comboBox2.SelectedIndex == -1 )
            {
                errorProvider1.SetError(comboBox2, "Mandatory");
                return false;
            }
            else
            {
                errorProvider1.SetError(comboBox2, "");
            }
            return true;
        }

        private bool validateEmail()
        {
            bool isEmail = Regex.IsMatch(textBox1.Text
                , @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z"
                , RegexOptions.IgnoreCase);
            if (!isEmail)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Incorrect Email";
                errorProvider1.SetError(textBox1, "Incorrect Email");
            }
            else if (textBox1.Text.Length >= 50)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Email too long";
                errorProvider1.SetError(textBox1, "Email too long");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163); ;
                toolStatus.Text = "Valid email";
                errorProvider1.SetError(textBox1, "");
                return true;
            }
            return false;
        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            validateEmail();
        }

        private bool validateName()
        {
            if (textBox2.Text.Length < 4)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Name too short";
                errorProvider1.SetError(textBox2, "Name too short");
            }
            else if (textBox2.Text.Length >= 35)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Name too long";
                errorProvider1.SetError(textBox2, "Name too long");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163); ;
                toolStatus.Text = "Correct name";
                errorProvider1.SetError(textBox2, "");
                return true;
            }
            return false;
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            validateName();
        }

        private bool validateSurname()
        {
            if (textBox3.Text.Length < 4)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Surename too short";
                errorProvider1.SetError(textBox3, "Surname too short");
            }
            else if (textBox3.Text.Length >= 55)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Surname too long";
                errorProvider1.SetError(textBox3, "Surname too long");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
                toolStatus.Text = "Correct surname";
                errorProvider1.SetError(textBox3, "");
                return true;
            }
            return false;
        }
        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            validateSurname();
        }

        private bool validatePass()
        {
            if(!textBox6.Enabled)
                return true;
            if (!textBox6.Text.Any(c => char.IsDigit(c)))
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Password needs a number";
                errorProvider1.SetError(textBox6, "Password needs a number");
            }
            else if (textBox6.Text.ToLower().Equals(textBox6.Text))
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Password needs a capital letter";
                errorProvider1.SetError(textBox6, "Password needs a capital letter");
            }
            else if (textBox6.Text.All(char.IsLetterOrDigit))
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Password needs non-alpha numeric";
                errorProvider1.SetError(textBox6, "Password needs non-alpha numeric");
            }
            else if (textBox6.Text.Length < 4)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Password too short";
                errorProvider1.SetError(textBox6, "Password too short");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
                toolStatus.Text = "Correct password";
                errorProvider1.SetError(textBox6, "");
                return true;
            }
            return false;
        }

        private void textBox6_Validating(object sender, CancelEventArgs e)
        {
            validatePass();
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            toolStatus.ForeColor = Color.Red;
            toolStatus.Text = "Must be number";
            errorProvider1.SetError(maskedTextBox1, "Must be number");
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            errorProvider1.SetError(maskedTextBox1, "");
        }


        private bool validateNumber()
        {

            if (maskedTextBox1.Text.Length < 9)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Number to short";
                errorProvider1.SetError(maskedTextBox1, "Number to short");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
                toolStatus.Text = "Correct number";
                errorProvider1.SetError(maskedTextBox1, "");
                return true;
            }
            return false;
        }
        private void maskedTextBox1_Validating(object sender, CancelEventArgs e)
        {
            validateNumber();
        }

        private bool validateSecondsPass()
        {
            if (!textBox11.Enabled)
                return true;
            if (textBox11 == textBox6)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Password does not match";
                errorProvider1.SetError(textBox11, "Password does not match");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
                toolStatus.Text = "Password ok";
                errorProvider1.SetError(textBox11, "");
                return true;
            }
            return false;
        }

        private void textBox11_Validating(object sender, CancelEventArgs e)
        {
            validateSecondsPass();
        }

        public bool validateStreet()
        {
            return validarCalle(textBox12) && validarCalle(textBox5);
        }
        public bool validarCalle(TextBox textBox)
        {
            if (textBox.Text.Length > 45)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Street too long";
                errorProvider1.SetError(textBox, "Street too long");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
                toolStatus.Text = "Street ok";
                errorProvider1.SetError(textBox, "");
                return true;
            }
            return false;
        }
        private void textBox12_Validating(object sender, CancelEventArgs e)
        {
            validarCalle(textBox12);
        }

        private void textBox5_Validating(object sender, CancelEventArgs e)
        {
            validarCalle(textBox5);
        }


        private bool validateID()
        {
            textBox4.Text = textBox4.Text.ToUpper();
            String aux = textBox4.Text;
            ///Convertimos las NIF de extranjeros a españoles por el formato
            if (textBox4.Text.Length > 1 && textBox4.Text[0] >= 'X' && textBox4.Text[0] <= 'Z')
            {
                aux = textBox4.Text.Substring(1);
                aux = ((char)textBox4.Text[0] - 'X') + aux;
            }

            if (textBox4.Text.Length != 9)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Bad ID Length";
                errorProvider1.SetError(textBox4, "Bad ID Length");
                return false;
            }
            else if (aux.Substring(0, aux.Length - 1).Any(x => char.IsLetter(x)) || !char.IsLetter(aux[8]))
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Badly formed ID!";
                errorProvider1.SetError(textBox4, "Badly formed ID!");
                return false;
            }

            int resto = Convert.ToInt32(aux.Substring(0, 8)) % 23;

            if (IDletters[resto] != aux[8])
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "DNI letter does not match!";
                errorProvider1.SetError(textBox4, "DNI letter does not match!");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
                toolStatus.Text = "DNI ok";
                errorProvider1.SetError(textBox4, "");
                return true;
            }

            return false;
        }

        private void textBox4_Validating(object sender, CancelEventArgs e)
        {
            validateID();
        }

        private bool validarCP()
        {
            if (maskedTextBox2.Text.Any(x => char.IsLetter(x)) || maskedTextBox2.Text.Contains(" "))
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Postal Code can only contain numbers !!";
                errorProvider1.SetError(maskedTextBox2, "Postal Code can only contain numbers !!");
            }
            else if (maskedTextBox2.Text.Length != 5)
            {
                toolStatus.ForeColor = Color.Red;
                toolStatus.Text = "Postal Code must have 5 numbers !!";
                errorProvider1.SetError(maskedTextBox2, "Postal Code must have 5 numbers !!");
            }
            else
            {
                toolStatus.ForeColor = Color.FromArgb(118, 85, 163);
                toolStatus.Text = "Correct Zip Code";
                errorProvider1.SetError(maskedTextBox2, "");
                return true;
            }
            return false;
        }

        private void maskedTextBox2_Validating(object sender, CancelEventArgs e)
        {
            validarCP();
        }

        private void comboBox1_ValueMemberChanged(object sender, EventArgs e)
        {
            comboBox2.Text = "Select town";
            villages = neg.readVillages( provices[comboBox1.SelectedIndex] ) ;
            comboBox2.Items.Clear();
            foreach (Village p in villages)
                comboBox2.Items.Add(p.nombre);
            this.toolStatus.Text = "Ready";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void maskedTextBox2_Enter(object sender, EventArgs e)
        {
            maskedTextBox2.Focus();
            maskedTextBox2.Select(0, 0);
        }
    }
}

