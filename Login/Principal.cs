﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class Principal : Form
    {
        private InsertUser ins;
        private ModifyUser mod;
        private Products prod;
        private Orders order;
        private bool isHidden;

        private DateTime timePased;
        public Principal(string userLogged)
        {
            InitializeComponent();
            isHidden = false;
            toolStripStatusLabel1.TextAlign = ContentAlignment.MiddleLeft;
            toolStripStatusLabel1.AutoSize = false;
            toolStripStatusLabel4.Text = "Loged as: " + userLogged;
            toolStripStatusLabel3.Text = "tmp";
            toolStripStatusLabel1.Width = 500;
            toolStripStatusLabel1.Text = "Ready";
            timePased = DateTime.Parse("00:00:00");
            ContextMenuStrip menuStrip = new ContextMenuStrip();
            ToolStripMenuItem menuItem = new ToolStripMenuItem("Show/Hide");
            menuItem.Click += new EventHandler(menuItem_Click);
            menuItem.Name = "Show/Hide";
            menuStrip.Items.Add(menuItem);
            notifyIcon1.ContextMenuStrip = menuStrip;

        }

        void menuItem_Click(object sender, EventArgs e)
        {
            ToolStripItem menuItem = (ToolStripItem)sender;
            if (menuItem.Name == "Show/Hide")
            {
                if (!isHidden)
                {
                    this.Hide();
                    isHidden = true;
                }
                else
                {
                    this.Show();
                    isHidden = false;
                }
            }
        }
        private void insertarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            ins = new InsertUser(toolStripStatusLabel1);
            ins.MdiParent = this;
            ins.ControlBox = false;
            ins.FormBorderStyle = FormBorderStyle.None;
            ins.Show();
        }

        public void closeForms()
        {
            this.Controls.Remove(panel1);

            for (int i = 0; i < this.MdiChildren.Count(); i++)
            {
                Form f = this.MdiChildren[i];
                f.Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel3.Text = "Time: " + DateTime.Now.ToString("HH:mm:ss");
            timePased = timePased.AddSeconds(1);
            toolStripStatusLabel2.Text = "Work time: " + timePased.ToString("HH:mm:ss"); ;
        }

        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Do you want to exit?", "Exit", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                e.Cancel = true;
        }

        private void exitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            mod = new ModifyUser(toolStripStatusLabel1, this);
            mod.MdiParent = this;
            mod.ControlBox = false;
            mod.FormBorderStyle = FormBorderStyle.None;
            mod.Show();
        }

        private void elimiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            mod = new ModifyUser(toolStripStatusLabel1, this);
            mod.MdiParent = this;
            mod.ControlBox = false;
            mod.FormBorderStyle = FormBorderStyle.None;
            mod.setDeleteMode();
            mod.Show();

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {

            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            prod = new Products(toolStripStatusLabel1);
            prod.MdiParent = this;
            prod.ControlBox = false;
            prod.FormBorderStyle = FormBorderStyle.None;
            prod.Show();
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            mod = new ModifyUser(toolStripStatusLabel1, this);
            mod.MdiParent = this;
            mod.ControlBox = false;
            mod.FormBorderStyle = FormBorderStyle.None;
            mod.setSelectMode();
            mod.Show();

        }

        private void consultaYModificaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            Invoice order = new Invoice(toolStripStatusLabel1);
            order.newOrder();
            order.MdiParent = this;
            order.ControlBox = false;
            order.FormBorderStyle = FormBorderStyle.None;
            order.Show();
        }

        private void estadísticasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            Statistics stats = new Statistics(toolStripStatusLabel1);
            stats.MdiParent = this;
            stats.ControlBox = false;
            stats.FormBorderStyle = FormBorderStyle.None;
            stats.Show();
        }

        private void facturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            Invoice inv = new Invoice(toolStripStatusLabel1);
            inv.MdiParent = this;
            inv.ControlBox = false;
            inv.FormBorderStyle = FormBorderStyle.None;
            inv.Show();
        }

        private void stockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            this.IsMdiContainer = false;
            this.IsMdiContainer = true;
            ReportWindowStock stats = new ReportWindowStock(toolStripStatusLabel1);
            stats.MdiParent = this;
            stats.ControlBox = false;
            stats.FormBorderStyle = FormBorderStyle.None;
            stats.Show();
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox1().Show();
        }
    }
}
