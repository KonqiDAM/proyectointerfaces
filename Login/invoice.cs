﻿using Capa_entidad;
using capa_negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class Invoice : Form
    {
        private List<Order> orders;
        private Business neg;
        private ToolStripStatusLabel statusLabel;
        private DataView dv;
        private bool isNewOrder;


        public Invoice(ToolStripStatusLabel statusLabel)
        {
            InitializeComponent();
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            statusLabel.Text = "Loading data";
            neg = Business.getBusiness();
            orders = neg.getOrders();
            this.statusLabel = statusLabel;
            isNewOrder = false;
            popTable();
            statusLabel.Text = "Ready";

        }

        private void popTable()
        {
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            orders = neg.getOrders();
            dataGridView1.DataSource = orders;

            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            dv = new DataView(dt);
            dataGridView1.DataSource = dv;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isNewOrder)
            {
                Orders order = new Orders(statusLabel, new Order(Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()), Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[1].Value.ToString()), dataGridView1.SelectedRows[0].Cells[2].Value.ToString()));
                order.MdiParent = base.MdiParent;
                order.Location = new Point(0, 0);
                order.ControlBox = false;
                order.FormBorderStyle = FormBorderStyle.None;
                order.Show();
            }
            else
            {
                String userid = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                int pedid = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                List<Linpedido> art = neg.getLinped().Where(x => x.PedidoID == pedid).ToList();
                User user = neg.getUsers().Where(x => x.usuarioID == userid).ToList()[0];
                List<Order> order = orders.Where(x => x.PedidoID == pedid).ToList();
                ReportWindow inf = new ReportWindow(order[0], art, user);
                inf.Show();
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            dv.RowFilter = "PedidoID LIKE '*" + textBox1.Text + "*'" +
                " and usuarioID LIKE '*" + textBox2.Text + "*'";

        }

        public void newOrder()
        {
            isNewOrder = true;
            button1.Text = "Modify Order";
            button2.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(neg.deleteOrder(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()))
            {
                statusLabel.Text = "Deleted order Nº" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            }
            else
            {
                statusLabel.Text = "Error on delete!";
                statusLabel.ForeColor = Color.Red;
            }
            popTable();
        }
    }
}
