﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Capa_entidad;
using capa_negocio;

namespace Login
{
    public partial class ModifyUser : Form
    {
        private Business neg;
        private List<User> usuarios;
        private DataView dv;
        private string[] filter;
        private ToolStripStatusLabel statusLabel;
        private bool deleteMode;
        private InsertUser ins;
        private Orders newOrder;
        private bool selectUserMode;
        private Principal padre;
        public ModifyUser(ToolStripStatusLabel statusLabel, Principal pp)
        {
            statusLabel.Text = "Loading";
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            deleteMode = false;
            selectUserMode = false;
            InitializeComponent();
            neg = Business.getBusiness();
            populateTable();
            filter = new string[4];
            this.statusLabel = statusLabel;
            padre = pp;
            statusLabel.Text = "Ready";
        }

        public void populateTable()
        {
            usuarios = neg.getUsers();
            dataGridView1.DataSource = usuarios;

            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                    dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            dv = new DataView(dt);
            dataGridView1.DataSource = dv;
            dataGridView1.Columns["password"].Visible = false;
            dataGridView1.Columns["calle2"].Visible = false;
            dataGridView1.Columns["usuarioid"].Visible = false;
            dataGridView1.Columns["puebloid"].Visible = false;
            dataGridView1.Columns["provinciaid"].Visible = false;
            dataGridView1.Columns["email"].Width = 150;
            dataGridView1.Columns["apellidos"].Width = 156;
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            dv.RowFilter = "nombre LIKE '*" + textBox1.Text + "*'" +
                " and apellidos LIKE '*" + textBox2.Text + "*'" +
                " and email LIKE '*" + textBox3.Text + "*'" +
                " and dni LIKE '*" + textBox4.Text + "*'";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if(selectUserMode)
            {
                newOrder = new Orders(statusLabel,
                    dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                newOrder.ControlBox = false;
                newOrder.FormBorderStyle = FormBorderStyle.None;
                newOrder.MdiParent = base.MdiParent;
                newOrder.Location = new Point(0, 0);
                newOrder.Show();

            }
            else if (!deleteMode)
            {
                ins = new InsertUser(statusLabel,
                    dataGridView1.SelectedRows[0].Cells[0].Value.ToString(), this);

                ins.ControlBox = false;
                ins.FormBorderStyle = FormBorderStyle.None;
                ins.MdiParent = base.MdiParent;
                ins.Location = new Point(0, 0);
                ins.Show();
            }
            else
            {
                ins = new InsertUser(statusLabel,
                    dataGridView1.SelectedRows[0].Cells[0].Value.ToString(), this);

                ins.ControlBox = false;
                ins.FormBorderStyle = FormBorderStyle.None;
                ins.MdiParent = base.MdiParent;
                ins.Location = new Point(0, 0);
                ins.setDeleteMode(this);
                ins.Show();
                populateTable();
            }
        }

        public void setDeleteMode()
        {
            button1.Text = "Delete";
            deleteMode = true;
        }

        public void setSelectMode()
        {
            selectUserMode = true;
            button1.Text = "Select";
        }
    }
}
