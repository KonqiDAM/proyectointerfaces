﻿using Capa_entidad;
using capa_negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class ReportWindow : Form
    {
        private Order order;
        private List<Linpedido> cart;
        private User user;
        private Business neg;
        private int total;
        public ReportWindow(Order order, List<Linpedido> cart, User user)
        {
            InitializeComponent();

            this.order = order;
            this.cart = cart;
            this.user = user;
            neg = Business.getBusiness();
        }

        private void ReportWindow_Load(object sender, EventArgs e)
        {

            reportViewer1.Dock = DockStyle.Fill;
            reportViewer1.LocalReport.ReportEmbeddedResource = "Login.Report1.rdlc";
            this.Controls.Add(reportViewer1);

            this.reportViewer1.RefreshReport();


            List<Order> orders = new List<Order>();
            orders.Add(order);
            List<User> users = new List<User>();
            users.Add(user);
            List<InvoiceData> invdata = new List<InvoiceData>();
            foreach (Article a in neg.getArticles())
            {
                foreach(Linpedido l in cart)
                {
                    if (l.articuloID == a.articuloID)
                        invdata.Add(new InvoiceData(l, a));

                }
            }
            foreach (Linpedido l in cart)
                total += l.importe;
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Order", orders));
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("Linped", invdata));
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("InvoiceData", invdata));

            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("User", users));

            reportViewer1.LocalReport.SetParameters(new ReportParameter("Total", total.ToString()));


            this.reportViewer1.RefreshReport();
        }
    }
}
