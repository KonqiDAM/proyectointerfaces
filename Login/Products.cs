﻿using Capa_entidad;
using capa_negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class Products : Form
    {
        private List<Article> products;
        private List<ArticleType> articleTypes;
        private Business neg;
        private DataView dv;
        private DataTable dt;
        private ToolStripStatusLabel statusLabel;
        private List<Label> labels;
        private List<TextBox> textBoxes;
        private List<Tv> tvs;
        private List<Memory> mem;
        private List<Camera> cam;
        private List<Lens> lens;
        public Products(ToolStripStatusLabel statusLabel)
        {
            InitializeComponent();
            statusLabel.Text = "Loading";
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            neg = Business.getBusiness();
            articleTypes = neg.getArticleTypes();
            tvs = neg.getTvs();
            mem = neg.getMemories();
            cam = neg.getCameras();
            lens = neg.getLens();
            labels = new List<Label>();
            textBoxes = new List<TextBox>();
            labels = new List<Label>();
            textBoxes.Add(new TextBox());
            labels.Add(new Label());
            this.statusLabel = statusLabel;
            popTable();
            statusLabel.Text = "Ready";
        }

        private void popTable()
        {
            products = neg.getArticles();
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("PVP", typeof(int)));
            dt.Columns.Add(new DataColumn("Brand", typeof(string)));
            dt.Columns.Add(new DataColumn("Type", typeof(string)));
            dt.Columns.Add(new DataColumn("id", typeof(string)));

            dv = new DataView(dt);
            dataGridView1.DataSource = dv;

            dt.Clear();

            for (int i = 0; i < products.Count; i++)
            {
                DataRow row = dt.NewRow();
                row["id"] = products[i].articuloID;
                row["Name"] = products[i].nombre;
                row["PVP"] = products[i].pvp;
                row["Brand"] = products[i].marcaID;
                foreach (ArticleType a in articleTypes)
                    if (a.tipoArticuloID == products[i].tipoArticuloID)
                        row["Type"] = a.Descripcion;

                dt.Rows.Add(row);

                row.AcceptChanges();
            }
            dataGridView1.Columns["id"].Visible = false;
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            dv.RowFilter = "Name LIKE '*" + textBox2.Text + "*'" +
                " and Type LIKE '*" + textBox1.Text + "*'";
            //dataGridView1_Click(null, null);
            if(dataGridView1.Rows.Count > 0)
                dataGridView1.Rows[0].Selected = true;

        }

        private void dinamicControlls()
        {
            labels.Clear();
            textBoxes.Clear();
            panel1.Controls.Clear();
            switch (dataGridView1.SelectedRows[0].Cells[3].Value.ToString())
            {
                case "Objetivo":
                    labels.Add(createLabel("tipo", "Type"));
                    textBoxes.Add(createTextBox("d1", lens.Where(t => t.ObjetivoID == textBox3.Text).ToList()[0].tipo));
                    textBoxes[0].Location = new Point(76, 0);
                    labels[0].Location = new Point(0, 0);
                    labels.Add(createLabel("especial", "Special"));
                    textBoxes.Add(createTextBox("d2", lens.Where(t => t.ObjetivoID == textBox3.Text).ToList()[0].especiales));
                    textBoxes[1].Location = new Point(76, 50);
                    labels[1].Location = new Point(0, 50);
                    labels.Add(createLabel("focal", "Focal"));
                    textBoxes.Add(createTextBox("d3", lens.Where(t => t.ObjetivoID == textBox3.Text).ToList()[0].focal));
                    textBoxes[2].Location = new Point(76, 100);
                    labels[2].Location = new Point(0, 100);
                    labels.Add(createLabel("apertura", "Aperture"));
                    textBoxes.Add(createTextBox("d4", lens.Where(t => t.ObjetivoID == textBox3.Text).ToList()[0].apertura));
                    textBoxes[3].Location = new Point(76, 150);
                    labels[3].Location = new Point(0, 150);

                    labels.Add(createLabel("montura", "Mount"));
                    textBoxes.Add(createTextBox("d5", lens.Where(t => t.ObjetivoID == textBox3.Text).ToList()[0].montura));
                    textBoxes[4].Location = new Point(76, 200);
                    labels[4].Location = new Point(0, 200);

                    break;

                case "TV":
                    labels.Add(createLabel("panel", "Panel"));
                    textBoxes.Add(createTextBox("d1", tvs.Where(t => t.TvID == textBox3.Text).ToList()[0].panel));
                    textBoxes[0].Location = new Point(76, 0);
                    labels[0].Location = new Point(0, 0);
                    labels.Add(createLabel("panel", "Screen"));
                    textBoxes.Add(createTextBox("d2", tvs.Where(t => t.TvID == textBox3.Text).ToList()[0].pantalla));
                    textBoxes[1].Location = new Point(76, 50);
                    labels[1].Location = new Point(0, 50);
                    labels.Add(createLabel("panel", "Res"));
                    textBoxes.Add(createTextBox("d3", tvs.Where(t => t.TvID == textBox3.Text).ToList()[0].resolucion));
                    textBoxes[2].Location = new Point(76, 100);
                    labels[2].Location = new Point(0, 100);
                    labels.Add(createLabel("panel", "TDT"));
                    textBoxes.Add(createTextBox("d4", tvs.Where(t => t.TvID == textBox3.Text).ToList()[0].tdt));
                    textBoxes[3].Location = new Point(76, 150);
                    labels[3].Location = new Point(0, 150);

                    labels.Add(createLabel("panel", "HD/FHD"));
                    textBoxes.Add(createTextBox("d5", tvs.Where(t => t.TvID == textBox3.Text).ToList()[0].hdreadyfullhd));
                    textBoxes[4].Location = new Point(76, 200);
                    labels[4].Location = new Point(0, 200);

                    break;

                case "Camara":
                    labels.Add(createLabel("resolucion", "Res"));
                    try
                    {
                        textBoxes.Add(createTextBox("d1", cam.Where(t => t.CamaraID == textBox3.Text).ToList()[0].resolucion));
                    } catch (Exception e)
                    {
                        textBoxes.Add(createTextBox("d1", ""));
                    }

                    textBoxes[0].Location = new Point(76, 0);
                    labels[0].Location = new Point(0, 0);
                    labels.Add(createLabel("sensor", "Sensor"));
                    try
                    { 
                        textBoxes.Add(createTextBox("d2", cam.Where(t => t.CamaraID == textBox3.Text).ToList()[0].sensor));
                    } catch (Exception e)
                    {
                        textBoxes.Add(createTextBox("d1", ""));
                    }
                    textBoxes[1].Location = new Point(76, 50);
                    labels[1].Location = new Point(0, 50);
                    labels.Add(createLabel("objetico", "Lens"));
                    try
                    { 
                        textBoxes.Add(createTextBox("d3", cam.Where(t => t.CamaraID == textBox3.Text).ToList()[0].objetivo));
                    } catch (Exception e)
                    {
                        textBoxes.Add(createTextBox("d1", ""));
                    }
                    textBoxes[2].Location = new Point(76, 100);
                    labels[2].Location = new Point(0, 100);
                    labels.Add(createLabel("tipos", "Types"));
                    try { 
                    textBoxes.Add(createTextBox("d4", cam.Where(t => t.CamaraID == textBox3.Text).ToList()[0].tipo));
                    } catch (Exception e)
                    {
                        textBoxes.Add(createTextBox("d1", ""));
                    }
                    textBoxes[3].Location = new Point(76, 150);
                    labels[3].Location = new Point(0, 150);

                    labels.Add(createLabel("factor", "Factor"));
                    try { 
                    textBoxes.Add(createTextBox("d5", cam.Where(t => t.CamaraID == textBox3.Text).ToList()[0].factor));
                    } catch (Exception e)
                    {
                        textBoxes.Add(createTextBox("d1", ""));
                    }
                    textBoxes[4].Location = new Point(76, 200);
                    labels[4].Location = new Point(0, 200);

                    

                    labels.Add(createLabel("pantalla", "Screen"));
                    try
                    {
                        textBoxes.Add(createTextBox("d7", cam.Where(t => t.CamaraID == textBox3.Text).ToList()[0].pantalla));
                    }
                    catch (Exception e)
                    {
                        textBoxes.Add(createTextBox("d1", ""));
                    }
                    textBoxes[5].Location = new Point(76, 250);
                    labels[5].Location = new Point(0, 250);

                    break;

                case "Memoria":
                    labels.Add(createLabel("tipo", "Type"));
                    textBoxes.Add(createTextBox("d1", mem.Where(m => m.MemoriaID == textBox3.Text).ToList()[0].tipo));
                    textBoxes[0].Location = new Point(76, 0);
                    labels[0].Location = new Point(0, 0);

                    break;

            }

            foreach (TextBox t in textBoxes)
                panel1.Controls.Add(t);
            foreach (Label l in labels)
                panel1.Controls.Add(l);

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (!textBox5.Text.All(c => c >= '0' && c <= '9') || textBox5.Text.Length == 0)
            {
                statusLabel.ForeColor = Color.Red;
                statusLabel.Text = "Only numbers are acepted!!!";
                button2.Enabled = false;
            }
            else
            {
                button2.Enabled = true;
                statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
                statusLabel.Text = "Correct price";
            }
        }


        public TextBox createTextBox(string name, string text)
        {
            TextBox t = new TextBox();
            t.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(196)))), ((int)(((byte)(206)))));
            t.Enabled = false;
            t.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            t.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(85)))), ((int)(((byte)(163)))));
            t.Name = name;
            t.Size = new System.Drawing.Size(386, 32);
            t.Text = text;
            return t;
        }

        public Label createLabel(string name, string text)
        {
            Label l = new Label();
            l.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F);
            l.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(85)))), ((int)(((byte)(163)))));
            l.Name = name;
            l.Size = new System.Drawing.Size(68, 29);
            l.AutoSize = true;
            l.Text = text;
            return l;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Article aux;
            aux = products.Where(x => x.articuloID == dataGridView1.SelectedRows[0].Cells[4].Value.ToString() ).ToList()[0];
            aux.pvp = Convert.ToInt32(textBox5.Text);
            if (neg.editPrice(aux))
            {
                statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
                statusLabel.Text = "Price updated!";
            }
            else
            {
                statusLabel.ForeColor = Color.Red;
                statusLabel.Text = "Error on price";
                int auxIndex = dataGridView1.SelectedRows[0].Index;
            }
            popTable();
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count != 0)
                {
                    textBox3.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
                    textBox4.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    textBox5.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                    textBox6.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                    textBox7.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                    button2.Enabled = false;
                    dinamicControlls();
                }
            }
            catch (Exception es)
            {
                ;
            }
            
        }

        private void dataGridView1_CurrentCellChanged(object sender, EventArgs e)
        {
            dataGridView1_Click(null, null);
        }
    }
}
