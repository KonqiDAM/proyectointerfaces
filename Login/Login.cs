﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capa_negocio;//es la capa_negocio

namespace Login
{
    public partial class USerLogin : Form
    {
        private int remainingAttempts;
        private Business neg;
        private Principal p;
        public USerLogin()
        {
            neg = Business.getBusiness();
            InitializeComponent();
            AcceptButton = button1;//Enter detection
            toolStripStatusLabel1.Text = "Enter username and password";
            remainingAttempts = 3;
            toolStripStatusLabel2.Text = "";

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Checking credentials";
            if (neg.validateUser(textBox1.Text, textBox2.Text))
            {
                toolStripStatusLabel1.Text = "Access granted";
                p = new Principal(textBox1.Text);

                Hide();
                p.Show();
            }
            else
            {
                toolStripStatusLabel1.Text = "Access denied";
                remainingAttempts--;
                if(remainingAttempts <= 0)
                    Application.Exit();
                toolStripStatusLabel2.Text = "Attempts remaining: " + remainingAttempts;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
