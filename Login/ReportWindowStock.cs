﻿using Capa_entidad;
using capa_negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class ReportWindowStock : Form
    {
        List<ArticleStock> artstock;
        List<Stock> stock;
        List<Article> articles;
        ToolStripStatusLabel statusLabel;
        public ReportWindowStock(ToolStripStatusLabel statusLabel)
        {
            InitializeComponent();
            this.statusLabel = statusLabel;
            statusLabel.Text = "Loading";
            statusLabel.ForeColor =Color.FromArgb(118, 85, 163);
            label10.TextAlign = ContentAlignment.MiddleCenter;
            artstock = new List<ArticleStock>();
            stock = Business.getBusiness().getStock();
            articles = Business.getBusiness().getArticles();
            statusLabel.Text = "Ok";
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            numericUpDown1.Value = 20;

        }

        private void ReportWindow_Load(object sender, EventArgs e)
        {
               
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            statusLabel.Text = "Filtering";
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            artstock.Clear();
            foreach (Stock s in stock)
            {
                foreach (Article a in articles)
                {
                    if (s.articuloID == a.articuloID && s.disponible <= numericUpDown1.Value)
                    {
                        artstock.Add(new ArticleStock(s.articuloID, a.nombre, a.pvp.ToString(), a.marcaID, a.imagen, a.urlimagen, a.especificaciones, a.tipoArticuloID.ToString(), s.disponible));
                    }
                }
            }

            reportViewer1.Dock = DockStyle.Fill;
            reportViewer1.LocalReport.ReportEmbeddedResource = "Login.Report2.rdlc";
            tableLayoutPanel1.Dock = DockStyle.None;
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("ArticlesStock", artstock));
            this.reportViewer1.RefreshReport();
            statusLabel.Text = "Ok";
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
        }
    }
}
