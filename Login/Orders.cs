﻿using Capa_entidad;
using capa_negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class Orders : Form
    {
        private List<Order> orders;
        private List<Article> products;
        private List<ArticleType> articleTypes;
        private List<Article> shopingCart;
        private List<Linpedido> lin;
        private Order order;

        private DataTable dt;
        private DataView dv;
        private Business neg;
        private ToolStripStatusLabel statusLabel;
        private bool isModifyOrder;


        public Orders(ToolStripStatusLabel statusLabel)
        {
            InitializeComponent();
            statusLabel.Text = "Loading";
            statusLabel.ForeColor = Color.FromArgb(118, 85, 163);
            this.statusLabel = statusLabel;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);
            neg = Business.getBusiness();
            articleTypes = neg.getArticleTypes();
            shopingCart = new List<Article>();
            popTableProducts();
            lin = neg.getLinped();
            isModifyOrder = false;
            statusLabel.Text = "Ready";

        }

        public Orders(ToolStripStatusLabel statusLabel, string userID) : this(statusLabel)
        {
            textBox3.Text = "Auto generated";
            textBox1.Text = userID;
            textBox2.Text = DateTime.Now.ToString();
            Location = new Point(50, 50);
        }

        public Orders(ToolStripStatusLabel statusLabel, Order order) : this(statusLabel)
        {
            this.order = order;
            textBox3.Text = order.PedidoID.ToString();
            textBox1.Text = order.usuarioID.ToString();
            textBox2.Text = DateTime.Now.ToString();
            Location = new Point(50, 50);
            updateCartFromOrder();
            isModifyOrder = true;
        }

        private void popTableProducts()
        {
            products = neg.getArticles();
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("PVP", typeof(int)));
            dt.Columns.Add(new DataColumn("Brand", typeof(string)));
            dt.Columns.Add(new DataColumn("Type", typeof(string)));
            dt.Columns.Add(new DataColumn("id", typeof(string)));

            dv = new DataView(dt);
            dataGridView2.DataSource = dv;

            dt.Clear();

            for (int i = 0; i < products.Count; i++)
            {
                DataRow row = dt.NewRow();
                row["id"] = products[i].articuloID;
                row["Name"] = products[i].nombre;
                row["PVP"] = products[i].pvp;
                row["Brand"] = products[i].marcaID;
                foreach (ArticleType a in articleTypes)
                    if (a.tipoArticuloID == products[i].tipoArticuloID)
                        row["Type"] = a.Descripcion;

                dt.Rows.Add(row);

                row.AcceptChanges();
            }
            dataGridView2.Columns["id"].Visible = false;
        }


        public void updateCart()
        {
            dataGridView3.DataSource = null;
            dataGridView3.Rows.Clear();
            dataGridView3.DataSource = shopingCart;
            
            dataGridView3.Columns["imagen"].Visible = false;
            dataGridView3.Columns["urlimagen"].Visible = false;
            dataGridView3.Columns["articuloID"].Visible = false;
            dataGridView3.Columns["especificaciones"].Visible = false;
        }

        private void product_filter(object sender, KeyEventArgs e)
        {
            dv.RowFilter = "Name LIKE '*" + textBox5.Text + "*'" +
                " and Type LIKE '*" + textBox4.Text + "*'";

            if (dataGridView2.Rows.Count > 0)
                dataGridView2.Rows[0].Selected = true;
        }

        private void dataGridView2_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView2.SelectedRows.Count > 0  && dataGridView2.Rows.Count > 0)
                {
                    button2.Enabled = true;
                }
                else
                {
                    button2.Enabled = false;
                }
            }
            catch (Exception es)
            {
                button2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < numericUpDown1.Value; i++)
            {
                shopingCart.Add(products.Where(x => x.articuloID == dataGridView2.SelectedRows[0].Cells[4].Value.ToString()).ToList()[0]);
            }
            updateCart();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            shopingCart.Clear();
            updateCart();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                shopingCart.RemoveAt(dataGridView3.SelectedRows[0].Index);
                updateCart();

            }
            catch (Exception) {; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //IR AL RESUMEN
            OrderSumary or;
            if (isModifyOrder)
                or = new OrderSumary(statusLabel, this, textBox1.Text, shopingCart, order);
            else
                or = new OrderSumary(statusLabel, this, textBox1.Text, shopingCart);
            or.ControlBox = false;
            or.MdiParent = base.MdiParent; ;
            or.ControlBox = false;
            or.FormBorderStyle = FormBorderStyle.None;
            or.Show();
        }

        private void updateCartFromOrder()
        {
            List<Linpedido> aux = 
            lin.Where(x => x.PedidoID ==  order.PedidoID).ToList();
            shopingCart.Clear();
            foreach (Linpedido l in aux)
                for (int i = 0; i < l.cantidad; i++)
                {
                    shopingCart.AddRange(products.Where(p => p.articuloID == l.articuloID).ToList());
                }
            updateCart();
        }
    }
}
