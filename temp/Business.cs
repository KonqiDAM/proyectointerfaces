﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using capa_datos;//Capa de datos
using Capa_entidad;
using System.Security.Cryptography;
using Capa_entidad;

namespace capa_negocio
{
    public class Business
    {
        private Data dat;
        static private Business neg;
        private Business()
        {
            dat = Data.getData();
        }


        public static Business getBusiness()
        {
            if (neg == null)
                neg = new Business();

            return neg;
        }

        public float calcIVA(float price)
        {
            return price * 1.21f;
        }

        public float calcPrice(List<Article> articles)
        {
            float price = 0;
            foreach(Article a in articles)
            {
                price += a.pvp;
            }
            return price;
        }

        public List<Stock> getStock()
        {
            return dat.readStock();
        }

        public bool validateUser(string usu, string pas)
        {
            pas = encode_MD5(pas);
            List<User> lista_usuarios = dat.readUsers();

            if (lista_usuarios != null)
            {
                for (int i = 0; i < lista_usuarios.Count; i++)
                {
                    if ((lista_usuarios[i].nombre == usu) &&
                        (lista_usuarios[i].password == pas))
                        return (true);
                }
            }

            return (false);
        }

        // Creo un nuevo usuario
        public bool newUser(User u)
        {
            u.usuarioID = (dat.readUsers().Select(x => Convert.ToInt32(x.usuarioID)).Max() + 1).ToString();//ID autoincremental
            u.password = encode_MD5(u.password);
            return (dat.newUser(u));
        }

        public int newOrder(string userID, List<Article> articles)
        {
            List<Article> aux = new List<Article>();
            int orderID = dat.readorders().Select(x => Convert.ToInt32(x.PedidoID)).Max() + 1;
            if( dat.newOrder(new Order(orderID, Convert.ToInt32(userID), DateTime.Now.ToString("yyyy-MM-dd HH:mm") )))
            {
                int i = 1;
                foreach(Article a in articles)
                {
                    if (!aux.Contains(a))
                    {
                        Console.WriteLine("Inserto:"+a.articuloID);
                        int repeted = articles.Where(o => o.articuloID == a.articuloID).Count();
                        dat.newLinPed(new Linpedido(orderID, i, a.articuloID, (int)a.pvp * repeted, repeted));
                        aux.Add(a);
                        i++;
                    }
                    else
                        Console.WriteLine("duplicado");
                }

                return orderID;
            }


            return -1;
        }

        
        public List<Province> readProvinces()
        {
            return dat.readProvinces();
        }
        
        public List<Village> readVillages()
        {
            return new HashSet<Village>(dat.readVillages()).ToList();
        }

        public List<ArticleType> getArticleTypes()
        {
            return dat.readArticleTypes();
        }

        public List<Tv> getTvs()
        {
            return dat.readTvs();
        }

        public List<Memory> getMemories()
        {
            return dat.readMemory();
        }

        public List<Camera> getCameras()
        {
            return dat.readCameras();
        }

        public List<Lens> getLens()
        {
            return dat.readLens();
        }

        public List<Linpedido> getLinped()
        {
            return dat.readLinpedid();
        }

        public List<Order> getOrders()
        {
            return dat.readorders();
        }
        //Village filtered by Province
        public List<Village> readVillages(Province prov)
        {
            List<Village> aux = new List<Village>();
            foreach(Village l in dat.readVillages()
                .Where(x => x.provinciaID == prov.ProvinceID)
                .Distinct().ToList())
            {
                if (!aux.Contains(l))
                    aux.Add(l);
            }

            return aux;

        }

        public List<Article> getArticles()
        {
            return dat.readArticles();
        }

        public List<User> getUsers()
        {
            return dat.readUsers();
        }

        public bool editUser(User u)
        {
            return dat.editUser(u);
        }

        public bool editPrice(Article a)
        {
            return dat.editPrice(a);
        }

        public bool deleteUser(User u)
        {
            
            return dat.deleteUser(u);
        }

        public bool deleteOrder(string id)
        {
            return dat.deleteOrder(id);
        }

        public int updateOrder(Order order, List<Article> articles)
        {
            if(dat.deleteOrder(order.PedidoID.ToString()))
                Console.WriteLine("Bien");
            Console.WriteLine(order.usuarioID.ToString());
            return newOrder(order.usuarioID.ToString(), articles);
        }


        public string encode_MD5(string pas)
        {
            int i;
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(pas));

            pas = "";
            for (i = 0; i < data.Length; i++)
            {
                pas = pas + data[i];
            }

            return pas;
        }
    }



    


}
