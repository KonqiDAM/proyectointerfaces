﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using capa_negocio;
using Capa_entidad;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace capa_presentacion_wpf
{
    /// <summary>
    /// Lógica de interacción para Users.xaml
    /// </summary>
    public partial class UserOrder : UserControl
    {
        Business neg;
        List<User> users;
        List<Village> villages;
        List<Province> provinces;
        Label statusBar;

        CollectionView view;
        bool searchUserMode;
        List<Order> pedidos;
        List<Linpedido> linped;
        List<Article> articles;
        private ObservableCollection<Order> listPedidos;
        private CollectionViewSource MiVista;
        private ObservableCollection<Linpedido> listArticles;
        private CollectionViewSource MiVista2;

        public UserOrder(Label statusBar, bool searchUser = false)
        {
            searchUserMode = searchUser;
            this.statusBar = statusBar;
            statusBar.Content = "Loading";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);
            InitializeComponent();
            neg = Business.getBusiness();
            pedidos = neg.getOrders();
            MiVista = (System.Windows.Data.CollectionViewSource)this.Resources["listPedidos"];
            listPedidos = new ObservableCollection<Order>();
            MiVista.Source = listPedidos;
            MiVista2 = (System.Windows.Data.CollectionViewSource)this.Resources["listArticles"];
            listArticles = new ObservableCollection<Linpedido>();
            MiVista2.Source = listArticles;
            users = neg.getUsers();
            villages = neg.readVillages();
            provinces = neg.readProvinces();
            linped = neg.getLinped();
            articles = neg.getArticles();
            emailFilter.Text = "";
            idFilter.Text = "";
            nameFilter.Text = "";
            surnameFilter.Text = "";
            actualizarVista();

            
            statusBar.Content = "Ready";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);

            if (searchUser)
                SearchUserMode();
        }

        private void SearchUserMode()
        {
            btModify.Content = "Modify order";
            btCreateUser.Content = "Select user";
        }

        private void actualizarVista()
        {
            users = neg.getUsers();

            ListBox1.DataContext = users;
            ListBox1.ItemsSource = users;

            view = CollectionViewSource.GetDefaultView(ListBox1.DataContext) as CollectionView;
            view.Filter = CustomFilter;

        }
  

        

        //btnew user
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (searchUserMode)
            {
                User auxUser = (User)ListBox1.SelectedItem;
                if (auxUser != null)
                {
                    StackPanel aux = (this.Parent as StackPanel);
                    aux.Children.Clear();
                    Pedidos ped = new Pedidos(statusBar, auxUser);
                    aux.Children.Add(ped);
                }
            }
        }

        //bt modificar
        private void btModify_Click(object sender, RoutedEventArgs e)
        {
            if (searchUserMode)
            {
                Order auxOrder = (Order)data.SelectedItem;
                User auxUser = (User)ListBox1.SelectedItem;
                if (auxOrder != null)
                {
                    StackPanel aux = (this.Parent as StackPanel);
                    aux.Children.Clear();
                    Pedidos ped = new Pedidos(statusBar, auxUser, auxOrder);
                    aux.Children.Add(ped);
                }
            }

        }

        private void nameFilter_KeyUp_1(object sender, KeyEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ListBox1.ItemsSource).Refresh();
        }

        private bool CustomFilter(object obj)
        {
            User aux = (User)obj;
            return (aux.nombre.ToLower().Contains(nameFilter.Text.ToLower()) || string.IsNullOrEmpty(nameFilter.Text))
                &&
                (aux.apellidos.ToLower().Contains(surnameFilter.Text.ToLower()) || string.IsNullOrEmpty(surnameFilter.Text))
                &&
                (aux.email.ToLower().Contains(emailFilter.Text.ToLower()) || string.IsNullOrEmpty(emailFilter.Text))
                &&
                (aux.dni.ToLower().Contains(idFilter.Text.ToLower()) || string.IsNullOrEmpty(idFilter.Text));
        }

        private void ListBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listPedidos.Clear();

            User auxUser = (User)ListBox1.SelectedItem;


            if (auxUser != null)
            {
                foreach(Order o in neg.getOrders())
                {
                    

                    if (o.usuarioID.ToString() == auxUser.usuarioID.ToString())
                    {
                        o.usuarioID = linped.Where(lin => lin.PedidoID == o.PedidoID).Sum(lin => lin.importe);
                        listPedidos.Add(o);

                    }
                }
            }
        }

        private void data_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //poner articulos
            listArticles.Clear();
            Order orderAux = (Order)data.SelectedItem;
            if (orderAux != null)
            {
                foreach (Linpedido o in neg.getLinped())
                {
                    if (o.PedidoID == orderAux.PedidoID)
                    {
                        try
                        {
                            o.PedidoID = o.cantidad * o.importe;

                            string aux = articles.Where(el => el.articuloID == o.articuloID).ToList()[0].nombre;
                            if (aux != null)
                                o.articuloID = aux;
                        }
                        catch (Exception) { }
                        listArticles.Add(o);
                    }
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Order auxOrder = (Order)data.SelectedItem;
            User auxUser = (User)ListBox1.SelectedItem;

            if (auxOrder != null && auxUser != null)
            {
                statusBar.Content = "Ok";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                StackPanel aux = (this.Parent as StackPanel);
                aux.Children.Clear();
                Informe1 st = new Informe1(auxOrder, auxUser);
                aux.Children.Add(st);
            }
            else
            {
                statusBar.Content = "Select a user and a order";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }

        }
    }
}
