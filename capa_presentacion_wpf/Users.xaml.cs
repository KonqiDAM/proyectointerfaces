﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using capa_negocio;
using Capa_entidad;
using System.Text.RegularExpressions;

namespace capa_presentacion_wpf
{
    /// <summary>
    /// Lógica de interacción para Users.xaml
    /// </summary>
    public partial class Users : UserControl
    {
        Business neg;
        List<User> users;
        List<Village> villages;
        List<Province> provinces;
        Label statusBar;
        bool creatingUser;
        bool isModifingUser;
        String IDletters;
        CollectionView view;
        bool searchUserMode;

        public Users(Label statusBar, bool searchUser = false)
        {
            searchUserMode = searchUser;
            this.statusBar = statusBar;
            statusBar.Content = "Loading";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);
            InitializeComponent();
            neg = Business.getBusiness();
            users = neg.getUsers();
            villages = neg.readVillages();
            provinces = neg.readProvinces();
            emailFilter.Text = "";
            idFilter.Text = "";
            nameFilter.Text = "";
            surnameFilter.Text = "";
            actualizarVista();

            diableFields();
            IDletters = "TRWAGMYFPDXBNJZSQVHLCKE";

            txDate.DisplayDateEnd = DateTime.Now;
            creatingUser = false;
            isModifingUser = false;
            comboxProvince.ItemsSource = neg.readProvinces();
            btConfirm.Visibility = Visibility.Hidden;
            btCancel.Visibility = Visibility.Hidden;
            
            statusBar.Content = "Ready";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);

            if (searchUser)
                SearchUserMode();
        }

        private void SearchUserMode()
        {
            btModify.Content = "Modify order";
            btDelete.Visibility = Visibility.Hidden;
            btCreateUser.Content = "Select user";
        }

        private void actualizarVista()
        {
            users = neg.getUsers();

            ListBox1.DataContext = users;
            ListBox1.ItemsSource = users;

            view = CollectionViewSource.GetDefaultView(ListBox1.DataContext) as CollectionView;
            view.Filter = CustomFilter;

        }

        private void diableFields(bool state = false)
        {
            txName.IsEnabled = txSurname.IsEnabled = txEmail.IsEnabled = txStreet.IsEnabled =
                txStreet2.IsEnabled = txPhone.IsEnabled = txCP.IsEnabled = txDNI.IsEnabled =
                txDate.IsEnabled = comboxProvince.IsEnabled = comboxTown.IsEnabled =
                txUserID.IsEnabled = txPassword.IsEnabled = txPass2.IsEnabled = state;
        }

        

        private void ListBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ListBox1.SelectedItem != null && !creatingUser)
            {
                User auxUser = (User)ListBox1.SelectedItem;
                txName.Text = auxUser.nombre;
                txSurname.Text = auxUser.apellidos;
                txPhone.Text = auxUser.telefono;
                txStreet.Text = auxUser.calle;
                txStreet2.Text = auxUser.calle2;
                txUserID.Text = auxUser.usuarioID;
                txDNI.Text = auxUser.dni;
                txCP.Text = auxUser.codpos;
                txEmail.Text = auxUser.email;
                if(auxUser.nacido != null)
                    txDate.SelectedDate = DateTime.Parse(auxUser.nacido);
                comboxProvince.SelectedIndex = searchProvice(auxUser);
                comboxTown.SelectedIndex = searchVillage(auxUser);
            }
        }

        //btnew user
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (searchUserMode)
            {
                User auxUser = (User)ListBox1.SelectedItem;
                if (auxUser != null)
                {
                    StackPanel aux = (this.Parent as StackPanel);
                    aux.Children.Clear();
                    Pedidos ped = new Pedidos(statusBar, auxUser);
                    aux.Children.Add(ped);
                }
            }
            else if (!creatingUser)
            {
                diableFields(true);

                ListBox1.IsEnabled = false;
                txName.Text = "";
                txSurname.Text = "";
                txPhone.Text = "";
                txStreet.Text = "";
                txStreet2.Text = "";
                txUserID.Text = "Autogenerated";
                txUserID.IsEnabled = false;
                txDNI.Text = "";
                txCP.Text = "";
                txEmail.Text = "";
                txDate.SelectedDate = DateTime.Now;
                idFilter.IsEnabled = false;
                nameFilter.IsEnabled = false;
                surnameFilter.IsEnabled = false;
                emailFilter.IsEnabled = false;
                idFilter.Text = "";
                nameFilter.Text = "";
                surnameFilter.Text = "";
                emailFilter.Text = "";
                creatingUser = true;
                btConfirm.Visibility = Visibility.Visible;
                btCancel.Visibility = Visibility.Visible;

                btCreateUser.Visibility = Visibility.Hidden;
                btModify.Visibility = Visibility.Hidden;
                btDelete.Visibility = Visibility.Hidden;
            }
        }

        //cancel bt
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ListBox1.IsEnabled = true;
            txName.Text = "";
            txSurname.Text = "";
            txPhone.Text = "";
            txStreet.Text = "";
            txStreet2.Text = "";
            txUserID.Text = "";
            txDNI.Text = "";
            txCP.Text = "";
            txEmail.Text = "";
            txDate.SelectedDate = DateTime.Now;
            idFilter.IsEnabled = true;
            nameFilter.IsEnabled = true;
            surnameFilter.IsEnabled = true;
            emailFilter.IsEnabled = true;
            idFilter.Text = "";
            nameFilter.Text = "";
            surnameFilter.Text = "";
            emailFilter.Text = "";
            diableFields(false);
            creatingUser = false;
            comboxTown.SelectedItem = null;
            comboxProvince.SelectedItem = null;
            btConfirm.Visibility = Visibility.Hidden;
            btCancel.Visibility = Visibility.Hidden;
            btCreateUser.Visibility = Visibility.Visible;
            btModify.Visibility = Visibility.Visible;
            btDelete.Visibility = Visibility.Visible;
            statusBar.Content = "Ok";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);

        }

        //confirm
        private void btConfirm_Click(object sender, RoutedEventArgs e)
        {
            
            if(validateAll())
            {
                statusBar.Content = "All data is ok, creating user...";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                String idVillage = villages[comboxTown.SelectedIndex].localidadID;
                String idProvice = ((Province) comboxProvince.SelectedItem).ProvinceID;
                User tmp = new User(null,
                    txEmail.Text,
                    txPassword.Password,
                    txName.Text,
                    txSurname.Text,
                    txDNI.Text,
                    txPhone.Text,
                    txStreet.Text,
                    txStreet2.Text,
                    txCP.Text,
                    idVillage,
                    idProvice,
                    txDate.DisplayDate.ToString("yyyy-MM-dd"));

                if (isModifingUser)
                {
                    tmp.usuarioID = txUserID.Text.ToString();

                    if (neg.editUser(tmp))
                    {
                        Button_Click_1(null, null);
                        isModifingUser = false;
                        statusBar.Content = "User updated correctly";
                        statusBar.Foreground = new SolidColorBrush(Colors.Black);
                        actualizarVista();
                    }
                    else
                    {
                        statusBar.Content = "Error on update";
                        statusBar.Foreground = new SolidColorBrush(Colors.Red);
                    }
                }
                else
                {

                    if (neg.newUser(tmp))
                    {

                        foreach (Control ctl in spanel1.Children)
                        {
                            if (ctl.GetType() == typeof(CheckBox))
                                ((CheckBox)ctl).IsChecked = false;
                            if (ctl.GetType() == typeof(TextBox))
                                ((TextBox)ctl).Text = String.Empty;
                        }
                        foreach (Control ctl in spanel2.Children)
                        {
                            if (ctl.GetType() == typeof(CheckBox))
                                ((CheckBox)ctl).IsChecked = false;
                            if (ctl.GetType() == typeof(TextBox))
                                ((TextBox)ctl).Text = String.Empty;
                            if (ctl.GetType() == typeof(PasswordBox))
                                ((PasswordBox)ctl).Password = String.Empty;
                        }
                        actualizarVista();
                        Button_Click_1(null, null);
                        statusBar.Content = "User created correctly";
                        statusBar.Foreground = new SolidColorBrush(Colors.Black);
                    }
                    else
                    {
                        statusBar.Content = "Error creating user!";
                        statusBar.Foreground = new SolidColorBrush(Colors.Red);
                    }
                }
            }
            else
            {
                statusBar.Content = "Data is wrong, chet it please!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
        }

        private void comboxProvince_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboxProvince.SelectedItem != null)
            {
                villages = neg.readVillages((Province)comboxProvince.SelectedItem);
                comboxTown.ItemsSource = villages;

            }
        }

        private int searchProvice(User aux)
        {
            for (int i = 0; i < provinces.Count; i++)
            {
                if (aux.provinciaID == provinces[i].ProvinceID)
                    return i;
            }
            return 0;
        }

        private int searchVillage(User aux)
        {
            
            for (int i = 0; i < villages.Count; i++)
            {
                if (aux.puebloID == villages[i].localidadID)
                    return i;
            }
            return 0;
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to do exit?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                if (neg.deleteUser((User)ListBox1.SelectedItem))
                {
                    statusBar.Content = "User deleted";
                    statusBar.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    statusBar.Content = "Error deleting, has orders!!";
                    statusBar.Foreground = new SolidColorBrush(Colors.Red);

                }
            }
            actualizarVista();
        }

        private bool validateAll()
        {
            return validateStreet()
                && validarCP()
                && validateID()
                && validateEmail()
                && validateName()
                && validateSurname()
                && validatePass()
                && txNumber_valid()
                && validateSecondsPass()
                && validateVillage()
                && txNumber_valid();
        }

        private bool validateVillage()
        {

            if (comboxProvince.SelectedIndex == -1)
            {
                statusBar.Content = "Province mandatory!!!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);


                return false;

            }
            else
            {
                statusBar.Content = "Ok";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
            }
            if (comboxTown.SelectedIndex == -1)
            {
                statusBar.Content = "Town mandatory!!!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
                return false;
            }
            else
            {
                statusBar.Content = "Ok";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
            }
            return true;
        }

        private bool validateEmail()
        {
            bool isEmail = Regex.IsMatch(txEmail.Text
                , @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z"
                , RegexOptions.IgnoreCase);
            if (!isEmail)
            {
                statusBar.Content = "Incorrect email!!!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if (txEmail.Text.Length >= 50)
            {
                statusBar.Content = "Email to long!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "Valid email!";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }

        private bool validateName()
        {
            if (txName.Text.Length < 4)
            {
                statusBar.Content = "Name to short";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if (txName.Text.Length >= 35)
            {
                statusBar.Content = "Name to long";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "Valid name";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }

        private bool validateSurname()
        {
            if (txSurname.Text.Length < 4)
            {
                statusBar.Content = "Surename to short";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if (txSurname.Text.Length >= 55)
            {
                statusBar.Content = "Surename to long!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "Correct surname";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }


        private bool validatePass()
        {
            if (!txPassword.IsEnabled)
                return true;
            if (!txPassword.Password.Any(c => char.IsDigit(c)))
            {
                statusBar.Content = "Password needs a number";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if (txPassword.Password.ToLower().Equals(txPassword.Password))
            {
                statusBar.Content = "Needs a capital letter!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if (txPassword.Password.All(char.IsLetterOrDigit))
            {
                statusBar.Content = "Needs a non alphanumeric!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if (txPassword.Password.Length < 4)
            {
                statusBar.Content = "Password to short";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "Valid password";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }

        private bool txNumber_valid()
        {
            if(txPhone.Text.Length < 9)
            {
                statusBar.Content = "Number to short";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if(txPhone.Text.Length > 12)
            {
                statusBar.Content = "Number to long";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if(txPhone.Text.All(char.IsDigit))
            {
                statusBar.Content = "Correct number";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }

        private bool validateSecondsPass()
        {
            if (!txPass2.IsEnabled)
                return true;
            if (txPass2.Password != txPassword.Password)
            {
                statusBar.Content = "Password does not match";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "Password match";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }

        public bool validateStreet()
        {
            return validarCalle(txStreet) && validarCalle(txStreet2);
        }
        public bool validarCalle(TextBox textBox)
        {
            if (textBox.Text.Length > 45)
            {
                statusBar.Content = "Street to long";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "Street ok";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }

        private bool validateID()
        {
            txDNI.Text = txDNI.Text.ToUpper();
            String aux = txDNI.Text;
            ///Convertimos las NIF de extranjeros a españoles por el formato
            if (txDNI.Text.Length > 1 && txDNI.Text[0] >= 'X' && txDNI.Text[0] <= 'Z')
            {
                aux = txDNI.Text.Substring(1);
                aux = ((char)txDNI.Text[0] - 'X') + aux;
            }

            if (txDNI.Text.Length != 9)
            {
                statusBar.Content = "Wromg ID length";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
                return false;
            }
            else if (aux.Substring(0, aux.Length - 1).Any(x => char.IsLetter(x)) || !char.IsLetter(aux[8]))
            {
                statusBar.Content = "Wrong ID format";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
                return false;
            }

            int resto = Convert.ToInt32(aux.Substring(0, 8)) % 23;

            if (IDletters[resto] != aux[8])
            {
                statusBar.Content = "ID letter does not match!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "ID ok!";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }

            return false;
        }


        private bool validarCP()
        {
            if (txCP.Text.Any(x => char.IsLetter(x)) || txCP.Text.Contains(" "))
            {
                statusBar.Content = "Postal Code can only have numbers!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else if (txCP.Text.Length != 5)
            {
                statusBar.Content = "Postal code must have 5 numbers!";
                statusBar.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                statusBar.Content = "Correct Postal code";
                statusBar.Foreground = new SolidColorBrush(Colors.Black);
                return true;
            }
            return false;
        }

        private void validateName(object sender, RoutedEventArgs e)
        {
            validateName();
        }

        private void txSurname_LostFocus(object sender, RoutedEventArgs e)
        {
            validateSurname();
        }

        private void txEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            validateEmail();
        }

        private void txStreet_LostFocus(object sender, RoutedEventArgs e)
        {
            validateStreet();
        }

        private void txStreet2_LostFocus(object sender, RoutedEventArgs e)
        {
            validateStreet();
        }

        private void txPhone_LostFocus(object sender, RoutedEventArgs e)
        {
            txNumber_valid();
        }

        private void txCP_LostFocus(object sender, RoutedEventArgs e)
        {
            validarCP();
        }

        private void txDNI_LostFocus(object sender, RoutedEventArgs e)
        {
            validateID();
        }


        private void comboxTown_LostFocus(object sender, RoutedEventArgs e)
        {
            validateVillage();
        }

        private void txPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            validatePass();
        }

        private void txPass2_LostFocus(object sender, RoutedEventArgs e)
        {
            validateSecondsPass();
        }


        //bt modificar
        private void btModify_Click(object sender, RoutedEventArgs e)
        {
            if (searchUserMode)
            {

            }
            else
            {
                diableFields(true);
                ListBox1.IsEnabled = false;
                btConfirm.Visibility = Visibility.Visible;
                btCancel.Visibility = Visibility.Visible;

                btCreateUser.Visibility = Visibility.Hidden;
                btModify.Visibility = Visibility.Hidden;
                btDelete.Visibility = Visibility.Hidden;
                txPass2.IsEnabled = false;
                txPassword.IsEnabled = false;
                isModifingUser = true;
            }

        }

        private void nameFilter_KeyUp_1(object sender, KeyEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ListBox1.ItemsSource).Refresh();
        }

        private bool CustomFilter(object obj)
        {
            User aux = (User)obj;
            return (aux.nombre.ToLower().Contains(nameFilter.Text.ToLower()) || string.IsNullOrEmpty(nameFilter.Text))
                &&
                (aux.apellidos.ToLower().Contains(surnameFilter.Text.ToLower()) || string.IsNullOrEmpty(surnameFilter.Text))
                &&
                (aux.email.ToLower().Contains(emailFilter.Text.ToLower()) || string.IsNullOrEmpty(emailFilter.Text))
                &&
                (aux.dni.ToLower().Contains(idFilter.Text.ToLower()) || string.IsNullOrEmpty(idFilter.Text));
        }
    }
}
