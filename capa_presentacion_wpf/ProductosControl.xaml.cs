﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using capa_negocio;
using Capa_entidad;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Windows.Markup;
using System.IO;
using System.Xml;

namespace capa_presentacion_wpf
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class ProductosControl : UserControl
    {
        List<Article> products;
        Label statusBar;
        Business neg;
        List <ArticleType> tipo;
        private List<Tv> tvs;
        private List<Memory> mem;
        private List<Camera> cam;
        private List<Lens> lens;

        private ObservableCollection<Article> l_productos;
        private CollectionViewSource MiVista;

        public ProductosControl(Label statusBar)
        {
            InitializeComponent();
            statusBar.Content = "Ok";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);

            extrPanelData.Children.RemoveAt(1);
            extrPanelData.Children.RemoveAt(1);

            this.statusBar = statusBar;
            neg = Business.getBusiness();
            products = neg.getArticles();
            lens = neg.getLens();
            tvs = neg.getTvs();
            mem = neg.getMemories();
            cam = neg.getCameras();
            MiVista = (System.Windows.Data.CollectionViewSource) this.Resources["listaProductos"];
            l_productos = new ObservableCollection<Article>();
            MiVista.Source = l_productos;
            tipo = neg.getArticleTypes();
            foreach (Article a in products)
            {
                foreach(ArticleType t in tipo)
                {
                    if (t.tipoArticuloID == a.tipoArticuloID)
                        a.imagen = t.Descripcion;
                }
                l_productos.Add(a);
            }
            pvpFilter.Text = "";
            brandFilter.Text = "";
            typeFilter.Text = "";

        }

        private void ListBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Label title = extraLabel;

            extrPanelData.Children.Clear();
            //title = extraLabel;
            extrPanelData.Children.Add(title);

            if (data.SelectedItem != null)
            {
                Article auxArticle = ((Article)data.SelectedItem);
                idBoxx.Text = auxArticle.articuloID;
                nameBox.Text = auxArticle.nombre;
                pvpBox.Text = auxArticle.pvp.ToString();
                brandBox.Text = auxArticle.marcaID;
                typeBOx.Text = auxArticle.imagen;
                switch (auxArticle.imagen)
                {
                    case "Objetivo":
                        Lens aux = null;
                        foreach (Lens l in lens)
                        {
                            if (l.ObjetivoID == auxArticle.articuloID)
                            {
                                aux = l;
                                break;
                            }
                        }

                        createLabel("Type");
                        createTextBox(aux.tipo);
                        createLabel("Mount");
                        createTextBox(aux.montura);
                        createLabel("Focus");
                        createTextBox(aux.focal);
                        createLabel("Aperture");
                        createTextBox(aux.apertura);
                        createLabel("Specials");
                        createTextBox(aux.especiales);
                        break;

                    case "TV":
                        Tv auxt = null;
                        foreach (Tv l in tvs)
                        {
                            if (l.TvID == auxArticle.articuloID)
                            {
                                auxt = l;
                                break;
                            }
                        }

                        createLabel("Panel");
                        createTextBox(auxt.panel);
                        createLabel("Screen");
                        createTextBox(auxt.pantalla);
                        createLabel("Resolution");
                        createTextBox(auxt.resolucion);
                        createLabel("TDT");
                        createTextBox(auxt.tdt);
                        createLabel("HD");
                        createTextBox(auxt.hdreadyfullhd);
                        break;


                    case "Memoria":
                        Memory auxm = null;
                        foreach (Memory l in mem)
                        {
                            if (l.MemoriaID == auxArticle.articuloID)
                            {
                                auxm = l;
                                break;
                            }
                        }

                        createLabel("Type");
                        createTextBox(auxm.tipo);
                        break;


                    case "Camara":
                        Camera auxc = null;
                        foreach (Camera l in cam)
                        {
                            if (l.CamaraID == auxArticle.articuloID)
                            {
                                auxc = l;
                                break;
                            }
                        }

                        createLabel("Objetive");
                        createTextBox(auxc.objetivo);
                        createLabel("Screen");
                        createTextBox(auxc.pantalla);
                        createLabel("Resolucion");
                        createTextBox(auxc.resolucion);
                        createLabel("Sensor");
                        createTextBox(auxc.sensor);
                        createLabel("Type");
                        createTextBox(auxc.tipo);
                        break;
                }
        }
        }


        private void Filtrar(object sender, FilterEventArgs e)
        {
            Article auxArt = (Article)e.Item;
            if (!pvpFilter.Text.All(char.IsDigit))
                pvpFilter.Text = "";
            if (auxArt != null)
            {
                try
                {
                    if ( 
                        (auxArt.nombre.ToLower().Contains(nameFilter.Text.ToLower()) || (nameFilter.Text == ""))
                         &&
                         ((pvpFilter.Text == "") || auxArt.pvp == Convert.ToInt32(pvpFilter.Text))
                         &&
                         ((brandFilter.Text == "") || auxArt.marcaID.ToLower().Contains(brandFilter.Text.ToLower()))
                         &&
                         ((typeFilter.Text == "") || auxArt.imagen.ToLower().Contains(typeFilter.Text.ToLower()))
                        )
                    {
                        e.Accepted = true;
                    }
                    else
                    {
                        e.Accepted = false;
                    }
                }catch(Exception)
                {

                    e.Accepted = false;
                }
            }
        }

        private void nameFilter_KeyUp(object sender, KeyEventArgs e)
        {
            MiVista.Filter += new FilterEventHandler(Filtrar);
        }

        private void createTextBox(string text)
        {
            TextBox textbox = DeepCopy(idBox);
            idBox.Text = text;

            extrPanelData.Children.Add(textbox);


        }
        private void createLabel(string text)
        {

            Label label = DeepCopy(idLabel);
            label.Content = text;

            extrPanelData.Children.Add(label);
        }

        public static Label DeepCopy(Label element)

        {

            var xaml = XamlWriter.Save(element);

            var xamlString = new StringReader(xaml);

            var xmlTextReader = new XmlTextReader(xamlString);

            var deepCopyObject = (Label)XamlReader.Load(xmlTextReader);

            return deepCopyObject;

        }

        public static TextBox DeepCopy(TextBox element)

        {

            var xaml = XamlWriter.Save(element);

            var xamlString = new StringReader(xaml);

            var xmlTextReader = new XmlTextReader(xamlString);

            var deepCopyObject = (TextBox)XamlReader.Load(xmlTextReader);

            return deepCopyObject;

        }

        private void pvpFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
        }
    }
}
