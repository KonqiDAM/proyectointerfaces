﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using capa_negocio;//es la capa_negocio


namespace capa_presentacion_wpf
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int remainingAttempts;
        private Business neg;
        bool autoLogin = false;
        public MainWindow()
        {
            InitializeComponent();
            textErrores.Content = "Ok";
            remainingAttempts = 3;
            neg = Business.getBusiness();


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            textErrores.Content = "Checking credentials";

            if (neg.validateUser(textUsername.Text, textPassword.Password))
            {
                textErrores.Content = "Login correcto";
                textErrores.Foreground = new SolidColorBrush(Colors.Black);
                Main p = new Main();
                p.Show();
                this.Hide();
            }
            else
            {
                textErrores.Content = "Acces denied";
                textErrores.Foreground = new SolidColorBrush(Colors.DarkRed);
                remainingAttempts--;
                if (remainingAttempts <= 0)
                    System.Windows.Application.Current.Shutdown();
                textErrores.Content = "Attempts remaining: " + remainingAttempts;
            }
        }

        private void detectEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Button_Click(null, null);
            }
        }
    }
}
