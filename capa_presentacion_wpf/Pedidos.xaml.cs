﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using capa_negocio;
using Capa_entidad;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Windows.Markup;
using System.IO;
using System.Xml;

namespace capa_presentacion_wpf
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class Pedidos : UserControl
    {
        List<Article> products;
        List<Article> carrito;
        Label statusBar;
        Business neg;
        List <ArticleType> tipo;
        private List<Tv> tvs;
        private List<Memory> mem;
        private List<Camera> cam;
        private List<Lens> lens;
        User auxUser;

        private ObservableCollection<Article> l_productos;
        private ObservableCollection<Article> l_carrito;

        private CollectionViewSource MiVista;
        private CollectionViewSource vistaCarrito;
        Order orderExistente;
        public Pedidos(Label statusBar, User usu, Order orderExistente = null)
        {
            InitializeComponent();
            
            auxUser = usu;
            statusBar.Content = "Ok";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);
            this.statusBar = statusBar;
            neg = Business.getBusiness();
            products = neg.getArticles();
            lens = neg.getLens();
            tvs = neg.getTvs();
            mem = neg.getMemories();
            cam = neg.getCameras();
            
            carrito = new List<Article>();
            MiVista = (System.Windows.Data.CollectionViewSource)this.Resources["listaProductos"];
            l_productos = new ObservableCollection<Article>();
            vistaCarrito = (System.Windows.Data.CollectionViewSource)this.Resources["carritoCompra"];
            l_carrito = new ObservableCollection<Article>();
            if (orderExistente != null)
            {
                this.orderExistente = orderExistente;
                foreach (Linpedido lin in neg.getLinped())
                {
                    foreach (Article ar in products)
                    {
                        if (ar.articuloID == lin.articuloID && lin.PedidoID == orderExistente.PedidoID)
                        {
                            for (int i = 0; i < lin.cantidad; i++)
                            {
                                carrito.Add(ar);
                                l_carrito.Add(ar);
                            }
                        }
                    }
                }
            }

            MiVista.Source = l_productos;
            vistaCarrito.Source = l_carrito;

            tipo = neg.getArticleTypes();
            foreach (Article a in products)
            {
                foreach(ArticleType t in tipo)
                {
                    if (t.tipoArticuloID == a.tipoArticuloID)
                        a.imagen = t.Descripcion;
                }
                l_productos.Add(a);
            }

            pvpFilter.Text = "";
            brandFilter.Text = "";
            typeFilter.Text = "";
            txAmount.Text = "1";
        }

        private void Filtrar(object sender, FilterEventArgs e)
        {
            Article auxArt = (Article)e.Item;
            if (!pvpFilter.Text.All(Char.IsDigit))
                pvpFilter.Text = "";
            if (auxArt != null)
            {
                try
                {
                    if ( 
                        (auxArt.nombre.ToLower().Contains(nameFilter.Text.ToLower()) || (nameFilter.Text == ""))
                         &&
                         ((pvpFilter.Text == "") || auxArt.pvp == Convert.ToInt32(pvpFilter.Text))
                         &&
                         ((brandFilter.Text == "") || auxArt.marcaID.ToLower().Contains(brandFilter.Text.ToLower()))
                         &&
                         ((typeFilter.Text == "") || auxArt.imagen.ToLower().Contains(typeFilter.Text.ToLower()))
                        )
                    {
                        e.Accepted = true;
                    }
                    else
                    {
                        e.Accepted = false;
                    }
                }catch(Exception)
                {
                    e.Accepted = false;
                }
            }
        }

        private void nameFilter_KeyUp(object sender, KeyEventArgs e)
        {
            MiVista.Filter += new FilterEventHandler(Filtrar);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Article aux = (Article)data.SelectedItem;
            if (aux != null)
            {
                for (int i = 0; i < Convert.ToInt32(txAmount.Text); i++)
                {
                    l_carrito.Add(aux);
                    carrito.Add(aux);
                }
            }
        }

        private void btCreateUser_Copy_Click(object sender, RoutedEventArgs e)
        {
            Article aux = (Article)listaCarrito.SelectedItem;
            if (aux != null)
            {
                l_carrito.Remove(aux);
                carrito.Remove(aux);
            }
        }

        private void btClearShopingCart_Click(object sender, RoutedEventArgs e)
        {
            l_carrito.Clear();
        }

        private void txAmount_KeyUp(object sender, KeyEventArgs e)
        {
            string aux = "";
            for (int i = 0; i < txAmount.Text.Length; i++)
            {
                if (txAmount.Text[i] >= '1' && txAmount.Text[i] <= '9')
                    aux += txAmount.Text[i];
            }
            txAmount.Text = aux;
            if (txAmount.Text == "")
                txAmount.Text = "1";
        }

        private void btGoSumaryScreen_Click(object sender, RoutedEventArgs e)
        {
            StackPanel aux = (this.Parent as StackPanel);
            aux.Children.Clear();
            OrderSumary orsu = new OrderSumary(statusBar, auxUser, carrito, orderExistente);
            aux.Children.Add(orsu);
        }
    }
}
