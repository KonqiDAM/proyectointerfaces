﻿using Capa_entidad;
using capa_negocio;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace capa_presentacion_wpf
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class Statistics : UserControl
    {
        Label statusBar;
        Business neg;
        int[] days;
        int[] typesAmount;
        List<Order> orders;
        List<Article> articles;
        List<Linpedido> linpedidos;

        public Statistics(Label statusBar)
        {
            InitializeComponent();
            this.statusBar = statusBar;
            statusBar.Content = "Ok";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);
            neg = Business.getBusiness();
            days = new int[31];
            typesAmount = new int[4];
            orders = neg.getOrders();
            articles = neg.getArticles();
            linpedidos = neg.getLinped();

        }

        private void comboxMonths_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int auxCount = 0;
            SeriesCollection serie2 = new SeriesCollection();

            for (int i = 0; i < days.Length; i++)
            {
                days[i] = 0;
            }
            for (int i = 0; i < typesAmount.Length; i++)
            {
                typesAmount[i] = 0;
            }
            foreach (Order o in orders)
            {
                DateTime d = DateTime.Parse(o.fecha);

                if (d.Month == comboxMonths.SelectedIndex + 1)
                {
                    days[d.Day - 1]++;
                    List<Linpedido> aux = linpedidos.Where(x => x.PedidoID == o.PedidoID).ToList();
                    foreach (Linpedido l in aux)
                    {
                        foreach (Article a in articles)
                        {
                            if (a.articuloID == l.articuloID)
                            {
                                typesAmount[a.tipoArticuloID - 1]++;
                                auxCount++;
                            }
                        }
                    }
                }
            }
            serie2.Add(new ColumnSeries
            {
                Title = "Sales",
                Values = new ChartValues<int>  ( days )
            });
            g2_eje_x.MaxValue = days.Length;
            List<string> daysS = new List<string>();
            for (int i = 0; i < days.Length; i++)
            {
                daysS.Add((1+i).ToString());

            }
            g2_eje_x.Labels = daysS;
            grafico2.Series = serie2;


            SeriesCollection serie4 = new SeriesCollection();
            serie4.Add(new PieSeries
            {
                Title = "TV",
                Values = new ChartValues<double> { typesAmount[0] },
                Stroke = System.Windows.Media.Brushes.HotPink,
                Fill = System.Windows.Media.Brushes.HotPink
            });
            serie4.Add(new PieSeries
            {
                Title = "Memory",
                Values = new ChartValues<double> { typesAmount[1] },
                Stroke = System.Windows.Media.Brushes.Green,
                Fill = System.Windows.Media.Brushes.Green
            });
            serie4.Add(new PieSeries
            {
                Title = "Camera",
                Values = new ChartValues<double> { typesAmount[2] },
                Stroke = System.Windows.Media.Brushes.Aquamarine,
                Fill = System.Windows.Media.Brushes.Aquamarine
            });            serie4.Add(new PieSeries
            {
                Title = "Lens",
                Values = new ChartValues<double> { typesAmount[3] },
                Stroke = System.Windows.Media.Brushes.BlueViolet,
                Fill = System.Windows.Media.Brushes.BlueViolet
            });
            SeriesCollection serie5 = new SeriesCollection();
            serie5.Add(new PieSeries
            {
                Title = "No data",
                Values = new ChartValues<double> { 1 },
                Stroke = System.Windows.Media.Brushes.HotPink,
                Fill = System.Windows.Media.Brushes.HotPink
            });
            if (auxCount == 0)
            {
                grafico4.Series = serie5;
            }
            else
                grafico4.Series = serie4;
        }
    }
}
