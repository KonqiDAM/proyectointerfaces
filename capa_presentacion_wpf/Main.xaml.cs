﻿using Capa_entidad;
using capa_negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace capa_presentacion_wpf
{
    /// <summary>
    /// Lógica de interacción para Main.xaml
    /// </summary>
    /// 
    public partial class Main : Window
    {
        System.Windows.Threading.DispatcherTimer dispatcherTimer;
        DateTime timePased;
        public Main()
        {
            InitializeComponent();
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
            timePased = DateTime.Parse("00:00:00");


            statusBar.Content = "OK";
            statusBar.Foreground = new SolidColorBrush(Colors.Black);
            statusBar2.Content = "User: admin";
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            timePased = timePased.AddSeconds(1);

            statusBar2.Content = "User: admin    Time since start: " + timePased.ToString("HH:mm:ss") + "    Time: " + DateTime.Now.ToString("HH: mm: ss");

        }

        private void btSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void gridIzquierdo_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to do exit?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }

        private void btUsuarios_Click(object sender, RoutedEventArgs e)
        {
            Users us = new Users(statusBar);
            panel1.Children.Clear();
            panel1.Children.Add(us);
        }

        private void btProductos_Click(object sender, RoutedEventArgs e)
        {
            ProductosControl us = new ProductosControl(statusBar);
            panel1.Children.Clear();
            panel1.Children.Add(us);
        }

        private void btSalir_Copy2_Click(object sender, RoutedEventArgs e)
        {
            UserOrder us = new UserOrder(statusBar, true);
            panel1.Children.Clear();
            panel1.Children.Add(us);
        }

        private void btSalir_Copy3_Click(object sender, RoutedEventArgs e)
        {
            Statistics st = new Statistics(statusBar);
            panel1.Children.Clear();
            panel1.Children.Add(st);
        }
    }
}
