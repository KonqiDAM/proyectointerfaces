﻿using Capa_entidad;
using capa_negocio;
using Syncfusion.Windows.Reports;
using Syncfusion.Windows.Reports.Viewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace capa_presentacion_wpf
{
    public partial class Informe1 : UserControl
    {
        public List<Article> articulos;
        private ReportViewer reportViewer1;
        Business neg;

        public Informe1(Order order, User user)
        {
            InitializeComponent();
            neg = Business.getBusiness();
            List<Order> orders = new List<Order>();
            orders.Add(order);
            List<User> users = new List<User>();
            users.Add(user);
            List<InvoiceData> invdata = new List<InvoiceData>();
            List<Linpedido> carrito = neg.getLinped().Where(lin => lin.PedidoID == order.PedidoID).ToList();
            foreach (Article a in neg.getArticles())
            {
                foreach (Linpedido l in carrito)
                {
                    if (l.articuloID == a.articuloID)
                        invdata.Add(new InvoiceData(l, a));

                }
            }
            int total = 0;
            foreach (Linpedido l in carrito)
                total += l.importe;
            users[0].calle2 = total.ToString();
            users[0].calle = (total*1.2).ToString();

            string ruta = System.IO.Directory.GetCurrentDirectory() +
                          "\\..\\..\\Report1.rdlc";
            reportViewer1 = new ReportViewer();
            reportViewer1.ReportPath = ruta;
            reportViewer1.ProcessingMode = ProcessingMode.Local;

            reportViewer1.DataSources.Clear();
            reportViewer1.DataSources.Add((new ReportDataSource("Order", orders)));
            reportViewer1.DataSources.Add((new ReportDataSource("Linped", carrito)));
            reportViewer1.DataSources.Add((new ReportDataSource("User", users)));
            reportViewer1.DataSources.Add((new ReportDataSource("InvoiceData", invdata)));
            //reportViewer1.DataSources.(new ReportParameter("Total", total.ToString()));

            reportViewer1.RefreshReport();
            g1.Children.Add(reportViewer1);
        }
    }
}
