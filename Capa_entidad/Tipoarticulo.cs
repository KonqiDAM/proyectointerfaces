﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Tipoarticulo
    {
        public Tipoarticulo(long tipotipoArticuloID, string descripcion)
        {
            this.tipotipoArticuloID = tipotipoArticuloID;
            Descripcion = descripcion;
        }

        public long tipotipoArticuloID { get; set; }
        public string Descripcion { get; set; }
    }
}
