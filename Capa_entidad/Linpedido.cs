﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Linpedido
    {
        public Linpedido(long pedidoID, long linea, string articuloID, float importe, long cantidad)
        {
            PedidoID = pedidoID;
            this.linea = linea;
            this.articuloID = articuloID;
            this.importe = importe;
            this.cantidad = cantidad;
        }

        public long PedidoID  { get; set; }
        public long linea { get; set; }
        public string articuloID { get; set; }
        public float importe { get; set; }
        public long cantidad { get; set; }
    }
}
