﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Stock
    {
        public Stock(string articuloID, long disponible, string entrega)
        {
            this.articuloID = articuloID;
            this.disponible = disponible;
            this.entrega = entrega;
        }

        public string articuloID { get; set; }
        public long disponible { get; set; }
        public string entrega { get; set; }
    }
}
