﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Memoria
    {
        public Memoria(string memoriaID, string tipo)
        {
            MemoriaID = memoriaID;
            this.tipo = tipo;
        }

        public string MemoriaID { get; set; }
        public string tipo { get; set; }
    }
}
