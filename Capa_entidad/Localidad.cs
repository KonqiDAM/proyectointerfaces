﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Localidad
    {
        public Localidad(string localidadID, string nombre, string provinciaID)
        {
            this.localidadID = localidadID;
            this.nombre = nombre;
            this.provinciaID = provinciaID;
        }

        public string localidadID { get; set; }
        public string nombre { get; set; }
        public string provinciaID { get; set; }
    }
}
