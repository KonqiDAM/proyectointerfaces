﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Articulo
    {
        public Articulo(string articuloID, string nombre, double pvp, string marcaID, string imagen, string urlimagen, string especificaciones, long tipoArticuloID)
        {
            this.articuloID = articuloID;
            this.nombre = nombre;
            this.pvp = pvp;
            this.marcaID = marcaID;
            this.imagen = imagen;
            this.urlimagen = urlimagen;
            this.especificaciones = especificaciones;
            this.tipoArticuloID = tipoArticuloID;
        }

        public string articuloID { get; set; }
        public string nombre { get; set; }
        public double pvp { get; set; }
        public string marcaID { get; set; }
        public string imagen { get; set; }
        public string urlimagen { get; set; }
        public string especificaciones { get; set; }
        public long tipoArticuloID { get; set; }

    }
}
