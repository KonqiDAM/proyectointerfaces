﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Pedido
    {
        public Pedido(long pedidoID, long usuarioID, string fecha)
        {
            PedidoID = pedidoID;
            this.usuarioID = usuarioID;
            this.fecha = fecha;
        }

        public long PedidoID { get; set; }
        public long usuarioID { get; set; }
        public string fecha { get; set; }

    }
}
