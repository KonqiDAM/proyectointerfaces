﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Marca
    {
        public Marca(string marcaID, string empresa, string logo)
        {
            this.marcaID = marcaID;
            this.empresa = empresa;
            this.logo = logo;
        }

        public string marcaID { get; set; }
        public string empresa { get; set; }
        public string logo { get; set; }
    }
}
