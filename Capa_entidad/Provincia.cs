﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capa_entidad
{
    class Provincia
    {
        public Provincia(string provinciaID, string nombre)
        {
            ProvinciaID = provinciaID;
            this.nombre = nombre;
        }

        public string ProvinciaID { get; set; }
        public string nombre { get; set; }
    }
}
